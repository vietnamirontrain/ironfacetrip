webpackJsonp([0],{

/***/ 198:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return EditProfilePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__global_global_variable__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_util_util__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_fire_storage__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common_http__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ionic_native_wheel_selector__ = __webpack_require__(215);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};









var EditProfilePage = /** @class */ (function () {
    function EditProfilePage(navCtrl, navParams, alertController, storage, util, db, http, camera, selector) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.alertController = alertController;
        this.storage = storage;
        this.util = util;
        this.db = db;
        this.http = http;
        this.camera = camera;
        this.selector = selector;
        this.countryList = [];
        this.user = __WEBPACK_IMPORTED_MODULE_2__global_global_variable__["a" /* GlobalVariables */].user;
        this.pictures = [];
        this.beforeImg = null;
        this.beforeSize = '0';
        this.afterImg = null;
        this.afterSize = '0';
        this.Guide = [
            { description: 'Free' },
            { description: '10 USD' },
            { description: '20 USD' },
            { description: '30 USD' },
            { description: '40 USD' },
            { description: '50 USD' },
            { description: '60 USD' },
            { description: '70 USD' },
            { description: '80 USD' },
            { description: '90 USD' },
            { description: '100 USD' },
            { description: '150 USD' },
            { description: '200 USD' },
            { description: '250 USD' },
            { description: '300 USD' },
            { description: '350 USD' },
            { description: '400 USD' },
            { description: '450 USD' },
            { description: '500 USD' },
            { description: '550 USD' },
            { description: '600 USD' },
            { description: '650 USD' },
            { description: '700 USD' },
            { description: '750 USD' },
            { description: '800 USD' },
            { description: '850 USD' },
            { description: '900 USD' },
            { description: '950 USD' },
            { description: '1000 USD' }
        ];
        this.loadCountryJSON();
        if (!this.user['pictures']) {
            this.user['pictures'] = '';
        }
        else {
            this.pictures = this.user['pictures'].split('|');
        }
    }
    /*-------------Start pick Guide---------------------*/
    EditProfilePage.prototype.openPicker = function () {
        var _this = this;
        this.selector.show({
            title: 'Select Your Guide',
            items: [
                this.Guide
            ],
            positiveButtonText: 'Done',
            negativeButtonText: 'cancel',
            defaultItems: [
                { index: 0, value: this.Guide[0].description }
            ]
        }).then(function (result) {
            _this.user['guide_fee'] = result[0].description;
        }, function (err) { return console.log('Error: ', err); });
    };
    /*-------------End pick Guide---------------------*/
    EditProfilePage.prototype.ionViewDidLoad = function () {
        this.itemsCollection = this.db.collection('users');
        this.user = this.navParams.get('user') || this.user;
    };
    EditProfilePage.prototype.onItemSelected = function (ev) {
        this.user['country'] = ev;
    };
    EditProfilePage.prototype.loadCountryJSON = function () {
        var _this = this;
        this.http
            .get('assets/country.json')
            .subscribe(function (data) {
            _this.itemsCountry = data;
        });
        this.http
            .get('assets/lang_list.json')
            .subscribe(function (data) {
            _this.itemsLang = data;
        });
        this.checkCountry = false;
        this.checkCity = false;
        this.checkLang1 = false;
        this.checkLang2 = false;
        this.checkLang3 = false;
        this.checkLang4 = false;
    };
    EditProfilePage.prototype.clickAutocomplete = function (data) {
        if (data.country) {
            this.user['country'] = data.country;
            this.user['city'] = data.city;
            this.filtContry = [];
            this.checkCountry = false;
        }
        if (data.city) {
            this.user['city'] = data.city;
            this.filtCity = [];
            this.checkCity = false;
        }
        if (data.language1) {
            this.user['language1'] = data.language1;
            this.filtLang = [];
            this.checkLang1 = false;
        }
        if (data.language2) {
            this.user['language2'] = data.language2;
            this.filtLang = [];
            this.checkLang2 = false;
        }
        if (data.language3) {
            this.user['language3'] = data.language3;
            this.filtLang = [];
            this.checkLang3 = false;
        }
        if (data.language4) {
            this.user['language4'] = data.language4;
            this.filtLang = [];
            this.checkLang4 = false;
        }
    };
    EditProfilePage.prototype.filter = function () {
        var _this = this;
        if (this.user['country'].length >= 3) {
            this.checkCountry = true;
            this.filtContry = this.itemsCountry.filter(function (item) { return item.country.toLowerCase().indexOf(_this.user['country'].toLowerCase()) > -1; }).map(function (i) { return i; });
        }
        else {
            this.filtContry = [];
            this.checkCountry = false;
        }
    };
    EditProfilePage.prototype.filterCity = function () {
        var _this = this;
        if (this.user['city'].length >= 2) {
            this.checkCity = true;
            this.filtCity = this.itemsCountry.filter(function (item) { return (item.city || '').toLowerCase().indexOf(_this.user['city'].toLowerCase()) > -1; });
        }
        else {
            this.checkCity = false;
            this.filtCity = [];
        }
    };
    EditProfilePage.prototype.filterLang = function () {
        var _this = this;
        if (this.user['language1'].length >= 1) {
            this.checkLang1 = true;
            this.filtLang = this.itemsLang.filter(function (item) { return item.lang_name.toLowerCase().indexOf(_this.user['language1'].toLowerCase()) > -1; });
        }
        else {
            this.checkLang1 = false;
            this.filtLang = [];
        }
    };
    EditProfilePage.prototype.filterLang2 = function () {
        var _this = this;
        if (this.user['language2'].length >= 1) {
            this.checkLang2 = true;
            this.filtLang = this.itemsLang.filter(function (item) { return item.lang_name.toLowerCase().indexOf(_this.user['language2'].toLowerCase()) > -1; });
        }
        else {
            this.checkLang2 = false;
            this.filtLang = [];
        }
    };
    EditProfilePage.prototype.filterLang3 = function () {
        var _this = this;
        if (this.user['language3'].length >= 1) {
            this.checkLang3 = true;
            this.filtLang = this.itemsLang.filter(function (item) { return item.lang_name.toLowerCase().indexOf(_this.user['language3'].toLowerCase()) > -1; });
        }
        else {
            this.checkLang3 = false;
            this.filtLang = [];
        }
    };
    EditProfilePage.prototype.filterLang4 = function () {
        var _this = this;
        if (this.user['language4'].length >= 1) {
            this.checkLang4 = true;
            this.filtLang = this.itemsLang.filter(function (item) { return item.lang_name.toLowerCase().indexOf(_this.user['language4'].toLowerCase()) > -1; });
        }
        else {
            this.checkLang4 = false;
            this.filtLang = [];
        }
    };
    EditProfilePage.prototype.presentAlertCheckbox = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var alert;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0: return [4 /*yield*/, this.alertController.create({
                            inputs: [
                                {
                                    type: 'checkbox',
                                    label: 'INTERNET GAME',
                                    value: 'INTERNET GAME ',
                                },
                                {
                                    type: 'checkbox',
                                    label: 'NIGHT LIFE',
                                    value: 'NIGHT LIFE  ',
                                },
                                {
                                    type: 'checkbox',
                                    label: 'PET',
                                    value: 'PET '
                                },
                                {
                                    type: 'checkbox',
                                    label: 'MUSIC',
                                    value: 'MUSIC '
                                },
                                {
                                    type: 'checkbox',
                                    label: 'MOVIE',
                                    value: 'MOVIE '
                                },
                                {
                                    type: 'checkbox',
                                    label: 'TRIP',
                                    value: 'TRIP '
                                },
                                {
                                    type: 'checkbox',
                                    label: 'SPORTS',
                                    value: 'SPORTS '
                                },
                                {
                                    type: 'checkbox',
                                    label: 'FOOD',
                                    value: 'FOOD '
                                },
                                {
                                    type: 'checkbox',
                                    label: 'SHOPPING',
                                    value: 'SHOPPING '
                                }
                            ],
                            buttons: [
                                {
                                    text: 'Cancel',
                                    role: 'cancel',
                                    cssClass: 'secondary',
                                    handler: function () {
                                    }
                                }, {
                                    text: 'Ok',
                                    handler: function (data) {
                                        _this.user['interest'] = data;
                                    }
                                }
                            ]
                        })];
                    case 1:
                        alert = _a.sent();
                        return [4 /*yield*/, alert.present()];
                    case 2:
                        _a.sent();
                        return [2 /*return*/];
                }
            });
        });
    };
    /*-------------reSize---------------------*/
    EditProfilePage.prototype.uploadFile = function (event) {
        var _this = this;
        var randomName = Math.random().toString(36).substring(2) + Date.now().toString(36);
        var realData = event.split(",")[1];
        var file = this.b64toBlob(realData, 'image/jpeg');
        var filePath = 'users/' + randomName + '.jpg';
        var ref = this.storage.ref(filePath);
        var task = ref.put(file);
        this.util.presentLoading();
        task.then(function (res) {
            _this.storage.ref(filePath).getDownloadURL().subscribe(function (data) {
                if (data) {
                    _this.pictures.push(data);
                    _this.user['pictures'] += data + '|';
                }
                _this.util.stopLoading();
            });
        }, function (err) { return _this.util.stopLoading(); });
    };
    EditProfilePage.prototype.loadImage = function () {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            correctOrientation: true,
            allowEdit: false
        };
        this.camera.getPicture(options).then(function (imageData) {
            var base64data = 'data:image/jpeg;base64,' + imageData;
            _this.beforeImg = base64data;
            _this.beforeSize = _this.getImageSize(_this.beforeImg);
            _this.createThumbnail();
        }, function (err) {
        });
    };
    EditProfilePage.prototype.createThumbnail = function () {
        var _this = this;
        this.generateFromImage(this.beforeImg, 400, 400, 10, function (data) {
            _this.afterImg = data;
            _this.afterSize = _this.getImageSize(_this.afterImg);
            _this.uploadFile(data);
        });
    };
    EditProfilePage.prototype.generateFromImage = function (img, MAX_WIDTH, MAX_HEIGHT, quality, callback) {
        if (MAX_WIDTH === void 0) { MAX_WIDTH = 500; }
        if (MAX_HEIGHT === void 0) { MAX_HEIGHT = 500; }
        if (quality === void 0) { quality = 5; }
        var canvas = document.createElement("canvas");
        var image = new Image();
        image.onload = function () {
            var width = image.width;
            var height = image.height;
            if (width > height) {
                if (width > MAX_WIDTH) {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH;
                }
            }
            else {
                if (height > MAX_HEIGHT) {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT;
                }
            }
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(image, 0, 0, width, height);
            var dataUrl = canvas.toDataURL('image/jpeg', quality);
            callback(dataUrl);
        };
        image.src = img;
    };
    EditProfilePage.prototype.getImageSize = function (data_url) {
        var head = 'data:image/jpeg;base64,';
        return ((data_url.length - head.length) * 3 / 4 / (1024 * 1024)).toFixed(4);
    };
    EditProfilePage.prototype.b64toBlob = function (b64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    };
    /*-------------End reSize-----------------*/
    EditProfilePage.prototype.save = function () {
        var _this = this;
        this.util.presentLoading();
        this.itemsCollection.doc(this.user['base64']).update(this.user).then(function (res) {
            __WEBPACK_IMPORTED_MODULE_2__global_global_variable__["a" /* GlobalVariables */].user = _this.user;
            _this.util.setLocal('user', _this.user);
            _this.util.stopLoading();
        }, function (err) { return _this.util.stopLoading(); });
    };
    EditProfilePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-edit-profile',template:/*ion-inline-start:"C:\Users\Kings\Desktop\project08092019\src\pages\edit-profile\edit-profile.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Edit Profile</ion-title>\n    <ion-buttons right>\n      <button ion-button clear (tap)="save()" [disabled]="!user[\'country\'] || !user[\'city\'] || !user[\'language1\']">\n        Save\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-list>\n    <div class="uploads">\n      <img *ngIf="!user[\'pictures\']" [src]="user[\'picture\']" alt="">\n      <ion-slides *ngIf="user[\'pictures\']" pager="true">\n        <ng-container *ngFor="let pic of pictures">\n          <ion-slide *ngIf="pic.length>0">\n            <img [src]="pic" alt="">\n          </ion-slide>\n        </ng-container>\n      </ion-slides>\n      <div class="upload-btn-wrapper">\n        <button class="btn" (click)="loadImage()">Upload a photo</button>\n        <!-- <input type="file" (change)="uploadFile($event)"> -->\n      </div>\n    </div>\n    \n    <div>\n      <ion-label>Country</ion-label>\n      <ion-searchbar [(ngModel)]="user[\'country\']" (ionInput)="filter()" placeholder="Country"></ion-searchbar>\n      <div *ngIf="user[\'country\'] != \'\' && checkCountry == true">\n        <ion-list class="search-css">\n          <div *ngFor=" let item of filtContry">\n            <ion-item (click)="clickAutocomplete(item)">\n              {{item.country}}, [{{item.city}}]\n            </ion-item>\n          </div>\n        </ion-list>\n      </div>\n    </div>\n    <div>\n      <ion-label>City</ion-label>\n      <ion-searchbar [(ngModel)]="user[\'city\']" (ionInput)="filterCity()" placeholder="city"></ion-searchbar>\n\n      <div *ngIf="user[\'city\'] != \'\' && checkCity == true">\n        <ion-list class="search-css">\n          <div *ngFor=" let item of filtCity">\n            <ion-item (click)="clickAutocomplete({\'city\':item.city})">\n              {{item.city}}, [{{item.country}}]\n            </ion-item>\n          </div>\n        </ion-list>\n      </div>\n    </div>\n    <div>\n      <ion-label>Language 1</ion-label>\n      <ion-searchbar [(ngModel)]="user[\'language1\']" (ionInput)="filterLang()" placeholder="language1"></ion-searchbar>\n\n      <div *ngIf="user[\'language1\'] != \'\' && checkLang1 == true">\n        <ion-list class="search-css">\n          <div *ngFor=" let item of filtLang">\n            <ion-item (click)="clickAutocomplete({\'language1\':item.lang_name})">\n              {{item.lang_name}}\n            </ion-item>\n          </div>\n        </ion-list>\n      </div>\n      <div *ngIf="user[\'language1\']">\n        <ion-label>Level Language 1</ion-label>\n        <md-radio-group [(ngModel)]="user[\'lvLanguage1\']" value="levelLanguage1">\n          <radio value="Level 1">Level 1</radio>\n          <radio value="Level 2">Level 2</radio>\n          <radio value="Level 3">Level 3</radio>\n          <radio value="Level 4">Level 4</radio>\n          <radio value="Level 5">Level 5</radio>\n        </md-radio-group>\n      </div>\n\n    </div>\n    <div *ngIf="user[\'language1\']">\n      <ion-label>Language 2 </ion-label>\n      <ion-searchbar [(ngModel)]="user[\'language2\']" (ionInput)="filterLang2()" placeholder="language2"></ion-searchbar>\n\n      <div *ngIf="user[\'language2\'] != \'\' && checkLang2 == true">\n        <ion-list class="search-css">\n          <div *ngFor=" let item of filtLang">\n            <ion-item (click)="clickAutocomplete({\'language2\':item.lang_name})">\n              {{item.lang_name}}\n            </ion-item>\n          </div>\n        </ion-list>\n      </div>\n      <div *ngIf="user[\'language2\']">\n        <ion-label>Level Language 2</ion-label>\n        <md-radio-group [(ngModel)]="user[\'lvLanguage2\']" value="levelLanguage2">\n          <radio value="Level 1">Level 1</radio>\n          <radio value="Level 2">Level 2</radio>\n          <radio value="Level 3">Level 3</radio>\n          <radio value="Level 4">Level 4</radio>\n          <radio value="Level 5">Level 5</radio>\n        </md-radio-group>\n      </div>\n    </div>\n    <div *ngIf="user[\'language2\']">\n      <ion-label>Language 3 </ion-label>\n      <ion-searchbar [(ngModel)]="user[\'language3\']" (ionInput)="filterLang3()" placeholder="language3"></ion-searchbar>\n\n      <div *ngIf="user[\'language3\'] != \'\' && checkLang3 == true">\n        <ion-list class="search-css">\n          <div *ngFor=" let item of filtLang">\n            <ion-item (click)="clickAutocomplete({\'language3\':item.lang_name})">\n              {{item.lang_name}}\n            </ion-item>\n          </div>\n        </ion-list>\n      </div>\n      <div *ngIf="user[\'language3\']">\n        <ion-label>Level Language 3</ion-label>\n        <md-radio-group [(ngModel)]="user[\'lvLanguage3\']" value="levelLanguage3">\n          <radio value="Level 1">Level 1</radio>\n          <radio value="Level 2">Level 2</radio>\n          <radio value="Level 3">Level 3</radio>\n          <radio value="Level 4">Level 4</radio>\n          <radio value="Level 5">Level 5</radio>\n        </md-radio-group>\n      </div>\n    </div>\n    <div *ngIf="user[\'language3\']">\n      <ion-label>Language 4 </ion-label>\n      <ion-searchbar [(ngModel)]="user[\'language4\']" (ionInput)="filterLang4()" placeholder="language4"></ion-searchbar>\n\n      <div *ngIf="user[\'language4\'] != \'\' && checkLang4 == true">\n        <ion-list class="search-css">\n          <div *ngFor=" let item of filtLang">\n            <ion-item (click)="clickAutocomplete({\'language4\':item.lang_name})">\n              {{item.lang_name}}\n            </ion-item>\n          </div>\n        </ion-list>\n      </div>\n      <div *ngIf="user[\'language4\']">\n        <ion-label>Level Language 4</ion-label>\n        <md-radio-group [(ngModel)]="user[\'lvLanguage4\']" value="levelLanguage4">\n          <radio value="Level 1">Level 1</radio>\n          <radio value="Level 2">Level 2</radio>\n          <radio value="Level 3">Level 3</radio>\n          <radio value="Level 4">Level 4</radio>\n          <radio value="Level 5">Level 5</radio>\n        </md-radio-group>\n      </div>\n    </div>\n\n    <div>\n        <ion-card>\n              <ion-label>Type of Member</ion-label>\n              <md-radio-group [(ngModel)]="user[\'typeMember\']" value="typeMember">\n                <radio value="true" placeholder="Select Guide Fee.">Guide</radio>\n                <radio value="false">Only Visitor</radio>\n              </md-radio-group>\n              <div *ngIf="user[\'typeMember\']===\'true\'" (click)="openPicker()">\n                <ion-label fixed>Guide Fee</ion-label>\n                <ion-item>\n                  <ion-input [(ngModel)]="user[\'guide_fee\']" disabled></ion-input>\n                </ion-item>\n              </div>\n        </ion-card>\n    </div>\n    <ion-card>\n        <ion-row>\n          <ion-col>\n              <ion-label>Meet at Airport?</ion-label>\n          </ion-col>\n          <ion-col>\n              <ion-item>\n                <ion-select class="possoble" [(ngModel)]="user[\'meet_at_airport\']">\n                  <ion-option value="true">possible</ion-option>\n                  <ion-option value="false">impossible</ion-option>\n                </ion-select>\n              </ion-item>\n            </ion-col>\n      </ion-row>\n    </ion-card>\n    <ion-card>\n        <ion-label>Interest</ion-label>\n      <ion-item>\n      <ion-input [(ngModel)]="user[\'interest\']" (tap)="presentAlertCheckbox()" placeholder="Select your interest"\n        disabled></ion-input>\n      </ion-item>\n    </ion-card>\n    <ion-card>\n        <ion-label>Introduction</ion-label>\n      <ion-item>\n      <ion-textarea rows="3" [(ngModel)]="user[\'introduction\']"\n        placeholder="Please introduce yourself to over 100 characters."></ion-textarea>\n        </ion-item>\n    </ion-card>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"C:\Users\Kings\Desktop\project08092019\src\pages\edit-profile\edit-profile.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__angular_fire_storage__["a" /* AngularFireStorage */],
            __WEBPACK_IMPORTED_MODULE_4__providers_util_util__["a" /* UtilProvider */],
            __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_6__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_8__ionic_native_wheel_selector__["a" /* WheelSelector */]])
    ], EditProfilePage);
    return EditProfilePage;
}());

//# sourceMappingURL=edit-profile.js.map

/***/ }),

/***/ 216:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterByPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FilterByPipe = /** @class */ (function () {
    function FilterByPipe() {
    }
    FilterByPipe_1 = FilterByPipe;
    FilterByPipe.isFoundOnWalking = function (value, key) {
        var walker = value;
        var found = false;
        do {
            if (walker.hasOwnProperty(key) ||
                Object.getOwnPropertyDescriptor(walker, key)) {
                found = true;
                break;
            }
        } while ((walker = Object.getPrototypeOf(walker)));
        return found;
    };
    FilterByPipe.isNumber = function (value) {
        return !isNaN(parseInt(value, 10)) && isFinite(value);
    };
    /**
     * Checks function's value if type is function otherwise same value
     */
    FilterByPipe.getValue = function (value) {
        return typeof value === 'function' ? value() : value;
    };
    FilterByPipe.prototype.filterByString = function (filter) {
        if (filter) {
            filter = filter.toLowerCase();
        }
        return function (value) {
            return !filter ||
                (value ? ('' + value).toLowerCase().indexOf(filter) !== -1 : false);
        };
    };
    FilterByPipe.prototype.filterByBoolean = function (filter) {
        return function (value) { return Boolean(value) === filter; };
    };
    FilterByPipe.prototype.filterByObject = function (filter) {
        var _this = this;
        return function (value) {
            for (var key in filter) {
                if (key === '$or') {
                    if (!_this.filterByOr(filter.$or)(FilterByPipe_1.getValue(value))) {
                        return false;
                    }
                    continue;
                }
                if (!value || !FilterByPipe_1.isFoundOnWalking(value, key)) {
                    return false;
                }
                if (!_this.isMatching(filter[key], FilterByPipe_1.getValue(value[key]))) {
                    return false;
                }
            }
            return true;
        };
    };
    FilterByPipe.prototype.isMatching = function (filter, val) {
        val = val.toLowerCase();
        switch (typeof filter) {
            case 'boolean':
                return this.filterByBoolean(filter)(val);
            case 'string':
                return this.filterByString(filter)(val);
            case 'object':
                return this.filterByObject(filter)(val);
        }
        return this.filterDefault(filter)(val);
    };
    /**
     * Filter value by $or
     */
    FilterByPipe.prototype.filterByOr = function (filter) {
        var _this = this;
        return function (value) {
            var length = filter.length;
            var arrayComparison = function (i) { return value.indexOf(filter[i]) !== -1; };
            var otherComparison = function (i) { return _this.isMatching(filter[i], value); };
            var comparison = Array.isArray(value)
                ? arrayComparison
                : otherComparison;
            for (var i = 0; i < length; i++) {
                if (comparison(i)) {
                    return true;
                }
            }
            return false;
        };
    };
    /**
     * Default filterDefault function
     */
    FilterByPipe.prototype.filterDefault = function (filter) {
        return function (value) { return filter === undefined || filter == value; };
    };
    FilterByPipe.prototype.transform = function (array, filter) {
        if (!array) {
            return array;
        }
        switch (typeof filter) {
            case 'boolean':
                return array.filter(this.filterByBoolean(filter));
            case 'string':
                if (FilterByPipe_1.isNumber(filter)) {
                    return array.filter(this.filterDefault(filter));
                }
                return array.filter(this.filterByString(filter));
            case 'object':
                return array.filter(this.filterByObject(filter));
            case 'function':
                return array.filter(filter);
        }
        return array.filter(this.filterDefault(filter));
    };
    FilterByPipe = FilterByPipe_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'filterBy',
            pure: false
        })
    ], FilterByPipe);
    return FilterByPipe;
    var FilterByPipe_1;
}());

//# sourceMappingURL=filter-by.js.map

/***/ }),

/***/ 250:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 250;

/***/ }),

/***/ 291:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 291;

/***/ }),

/***/ 33:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return UtilProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular_components_loading_loading_controller__ = __webpack_require__(170);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_modal_modal_controller__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_alert_alert_controller__ = __webpack_require__(156);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular_components_toast_toast_controller__ = __webpack_require__(193);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







/*
  Generated class for the UtilProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
var UtilProvider = /** @class */ (function () {
    function UtilProvider(loadingCtrl, alertCtrl, toastCtrl, events, storage, modalCtrl) {
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.events = events;
        this.storage = storage;
        this.modalCtrl = modalCtrl;
    }
    UtilProvider.prototype.showToast = function (message) {
        var _this = this;
        return new Promise(function (resolve) {
            var toast = _this.toastCtrl.create({
                message: message,
                duration: 3000,
                position: 'top'
            });
            toast.onDidDismiss(function () {
                console.log('Dismissed toast');
            });
            toast.present();
        });
    };
    /**
     * Show Loading
     *
     *
     * @memberOf HelpToolProvider
     */
    UtilProvider.prototype.presentLoading = function () {
        this.loader = this.loadingCtrl.create({
            spinner: 'crescent',
            content: 'Data loading...'
        });
        this.loader.present();
    };
    /**
     * Stop Show Loading
     *
     *
     * @memberOf HelpToolProvider
     */
    UtilProvider.prototype.stopLoading = function () {
        this.loader.dismiss();
    };
    UtilProvider.prototype.setLocal = function (key, value) {
        return this.storage.set(key, value);
    };
    UtilProvider.prototype.getLocal = function (key) {
        return this.storage.get(key);
    };
    UtilProvider.prototype.clearLocal = function () {
        return this.storage.clear();
    };
    /**
   * show modal
   * @param component
   * @param data
   */
    UtilProvider.prototype.showModal = function (component, data, cssClass) {
        var _this = this;
        if (cssClass === void 0) { cssClass = 'full'; }
        return new Promise(function (resolve) {
            var modal = null;
            var opts = {};
            if (cssClass) {
                opts['cssClass'] = cssClass;
            }
            modal = _this.modalCtrl.create(component, data, opts);
            modal.onDidDismiss(function (_data) {
                resolve(_data);
            });
            modal.present();
        });
    };
    UtilProvider.prototype.showAlert = function (title, subTitle) {
        this.alertCtrl.create({
            title: title,
            subTitle: subTitle,
            buttons: ['Close']
        }).present();
    };
    UtilProvider.prototype.confirm = function (title, subTitle) {
        var _this = this;
        return new Promise(function (resolve) {
            var alert = _this.alertCtrl.create({
                title: title,
                message: subTitle,
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: function () {
                            resolve('no');
                        }
                    },
                    {
                        text: 'Remove',
                        handler: function () {
                            resolve('yes');
                        }
                    }
                ]
            });
            alert.present();
        });
    };
    UtilProvider.prototype.showConfirm = function (title, subTitle) {
        var _this = this;
        return new Promise(function (resolve) {
            _this.alertCtrl.create({
                title: title,
                subTitle: subTitle,
                inputs: [
                    {
                        name: 'amount',
                        placeholder: 'Amount that you have',
                        type: 'number'
                    },
                    {
                        name: 'invested',
                        placeholder: 'Total invested (optional)',
                        type: 'number'
                    }
                ],
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: function (data) {
                            resolve();
                        }
                    },
                    {
                        text: 'Add',
                        handler: function (data) {
                            resolve(data);
                        }
                    }
                ]
            }).present();
        });
    };
    UtilProvider.prototype.mapCMCToCPC = function (coinmarketcap, cryptocompare) {
        if (!coinmarketcap)
            return null;
        //Convert an object with keys into an array of objects
        cryptocompare = Object.keys(cryptocompare).map(function (i) { return cryptocompare[i]; });
        var ignoreSpaceRegex = /\s/g, nonAlphaNumericRegex = /\W+/g;
        var map = {}, symbol1, name1, symbol2, name2, reduced;
        //Loop through every item in coinmarketcap
        //Note that symbols such as BTM, KNC will be repeated multiple times
        //Get the symbol of the current coin on coinmarketcap
        symbol1 = coinmarketcap.symbol;
        name1 = coinmarketcap.name.trim().replace(ignoreSpaceRegex, "").toLowerCase();
        // if(coinmarketcap[i].rank < 700){
        //Loop through every item on cryptocompare
        for (var j = cryptocompare.length - 1; j >= 0; j--) {
            //Get the symbol of the current coin on cryptocompare
            symbol2 = cryptocompare[j].Symbol;
            reduced = symbol2.replace(nonAlphaNumericRegex, "");
            name2 = cryptocompare[j].CoinName.trim().replace(ignoreSpaceRegex, "").toLowerCase();
            if (reduced.indexOf(symbol1) >= 0 && name1 === name2) {
                map[symbol1] = symbol2;
            }
            if (symbol1.toLowerCase() == 'trx') {
                if (symbol2.toLowerCase() == 'trx') {
                    map[symbol1] = symbol2;
                }
            }
        }
        return map;
    };
    UtilProvider.prototype.ValidURL = function (str) {
        var expression = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi;
        var regexp = new RegExp(expression);
        return regexp.test(str);
    };
    UtilProvider.prototype.extractUrl = function (str) {
        return str.match(/(https?:\/\/[^ ]*)/)[0];
    };
    UtilProvider.prototype.removeSpecialChar = function (str) {
        return str.replace(/[^a-zA-Z0-9]/g, '_');
    };
    UtilProvider.prototype.scrollTo = function (element, to, duration) {
        var start = element.scrollTop, change = to - start, currentTime = 0, increment = 20;
        var easeInOutQuad = function (t, b, c, d) {
            t /= d / 2;
            if (t < 1)
                return c / 2 * t * t + b;
            t--;
            return -c / 2 * (t * (t - 2) - 1) + b;
        };
        var animateScroll = function () {
            currentTime += increment;
            var val = easeInOutQuad(currentTime, start, change, duration);
            element.scrollTop = val;
            if (currentTime < duration) {
                setTimeout(animateScroll, increment);
            }
        };
        animateScroll();
    };
    UtilProvider.prototype.registerEvent = function (name, callback) {
        // this.events.unsubscribe(name);
        this.events.subscribe(name, callback);
    };
    UtilProvider.prototype.publishEvent = function (name, params) {
        this.events.publish(name, params);
    };
    UtilProvider.prototype.generateEmail = function () {
        var chars = 'abcdefghijklmnopqrstuvwxyz1234567890';
        var string = 'cust_';
        for (var ii = 0; ii < 8; ii++) {
            string += chars[Math.floor(Math.random() * chars.length)];
        }
        return (string + '@gmail.com');
    };
    UtilProvider.prototype.getChatId = function (id1, id2) {
        var chatId = '';
        if (id1 < id2) {
            chatId = id1 + id2;
        }
        else {
            chatId = id2 + id1;
        }
        return chatId;
    };
    UtilProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular_components_loading_loading_controller__["a" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_alert_alert_controller__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular_components_toast_toast_controller__["a" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular_components_modal_modal_controller__["a" /* ModalController */]])
    ], UtilProvider);
    return UtilProvider;
}());

//# sourceMappingURL=util.js.map

/***/ }),

/***/ 332:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__global_global_variable__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__edit_profile_edit_profile__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__homestay_detail_homestay_detail__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular_navigation_nav_params__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__login_login__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__tabs_tabs__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_util_util__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_app_app__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_facebook__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_storage__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};












var MePage = /** @class */ (function () {
    function MePage(app, alertCtrl, storage, navCtrl, navParams, modalCtrl, fb, appPrtvider, util) {
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modalCtrl = modalCtrl;
        this.fb = fb;
        this.appPrtvider = appPrtvider;
        this.util = util;
        this.localList = [];
    }
    MePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.get('checkUser').then(function (val) {
            if (val == true) {
                _this.checkUser = true;
                _this.user = __WEBPACK_IMPORTED_MODULE_2__global_global_variable__["a" /* GlobalVariables */].user;
            }
            else {
                _this.checkUser = false;
                _this.showConfirm();
            }
        });
    };
    MePage.prototype.editProfile = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__edit_profile_edit_profile__["a" /* EditProfilePage */], { user: this.user });
    };
    MePage.prototype.logout = function () {
        var _this = this;
        this.appPrtvider.setPlayerIDNull(this.user['base64']).then(function () {
            _this.util.clearLocal();
            _this.fb.logout();
            _this.storage.set('checkUser', false);
            __WEBPACK_IMPORTED_MODULE_2__global_global_variable__["a" /* GlobalVariables */].user = '';
            _this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_7__tabs_tabs__["a" /* TabsPage */]);
        });
    };
    MePage.prototype.viewDetail = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_4__homestay_detail_homestay_detail__["a" /* HomestayDetailPage */], { homestay: item });
    };
    MePage.prototype.showConfirm = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Login',
            message: 'Please login to continue !!!',
            buttons: [
                {
                    text: 'Disagree',
                    handler: function () {
                        // this.navCtrl.push(HomePage);
                        _this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_7__tabs_tabs__["a" /* TabsPage */]);
                    }
                },
                {
                    text: 'Agree',
                    handler: function () {
                        _this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_6__login_login__["a" /* LoginPage */]);
                    }
                }
            ]
        });
        confirm.present();
    };
    MePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-me',template:/*ion-inline-start:"C:\Users\Kings\Desktop\project08092019\src\pages\me\me.html"*/'<ion-header>\n  <ion-navbar color="color-header">\n    <ion-title>\n      FACE TRIP\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div *ngIf=\'checkUser==true\'>\n    <ion-card>\n      <ion-card-content>\n        <ion-grid>\n          <ion-row>\n            <ion-col col-4 text-center>\n              <img [src]="user[\'picture\']+\'?type=normal\'" alt="">\n            </ion-col>\n            <ion-col col-8>\n              <ion-grid>\n                <ion-row>\n                  <ion-col>\n                    <b>{{user[\'name\']}}</b>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <span>{{user[\'city\']}}, {{user[\'country\']}}</span>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <span class="langs">{{user[\'language1\']}}</span>\n                    <span class="langs">{{user[\'language2\']}}</span>\n                    <span class="langs">{{user[\'language3\']}}</span>\n                    <span class="langs">{{user[\'language4\']}}</span>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <span *ngIf="!user[\'guide_fee\'] || user[\'guide_fee\']==\'Free\' ; else elseGuide">Fee: Free</span>\n                    <ng-template #elseGuide>\n                      <span>Fee: ${{user[\'guide_fee\']}}</span>\n                    </ng-template>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <span *ngIf="user[\'meet_at_airport\'] === \'true\'; else elseairport">Meet at the Airport: POSSIBLE</span>\n                    <ng-template #elseairport>\n                      <span>Meet at the Airport: IMPOSSIBLE</span>\n                    </ng-template>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <span>INTEREST: </span>\n                    <span>{{user[\'interest\']}}</span>\n                  </ion-col>\n                </ion-row>\n                <ion-row>\n                  <ion-col>\n                    <span>{{user[\'introduction\']}}</span>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n            </ion-col>\n          </ion-row>\n          <ion-row>\n            <ion-col text-right style="justify-content: flex-end">\n              <button clear ion-button (tap)="logout()">Logout</button>\n            </ion-col>\n            <ion-col text-right style="justify-content: flex-end">\n              <button clear ion-button (tap)="editProfile()">Edit</button>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-card-content>\n    </ion-card>\n    <div *ngIf="user[\'homestay\']; else elsehomestay">\n      <ion-list>\n        <ion-item-divider>\n          Homestay\n        </ion-item-divider>\n        <ion-item (tap)="viewDetail(user[\'homestay\'])">\n          <ion-thumbnail item-start>\n            <img [src]="user[\'homestay\'][\'picture\']">\n          </ion-thumbnail>\n          <h3>{{user[\'homestay\'][\'location\']}}</h3>\n          <p><span>Price:</span> {{user[\'homestay\'][\'price\']}}</p>\n          <p> <span>Wifi:</span> {{user[\'homestay\'][\'wifi\']?\'Yes\': \'No\'}}</p>\n          <p> <span>Breakfast:</span> {{user[\'homestay\'][\'breakfast\']?\'Yes\': \'No\'}}</p>\n          <p>{{user[\'homestay\'][\'description\']}}</p>\n        </ion-item>\n      </ion-list>\n    </div>\n    <ng-template #elsehomestay>\n      <ion-fab right bottom>\n        <button (tap)="viewDetail()" ion-fab color="primary">\n          <ion-icon name="md-add"></ion-icon>\n        </button>\n      </ion-fab>\n    </ng-template>\n\n  </div>\n</ion-content>'/*ion-inline-end:"C:\Users\Kings\Desktop\project08092019\src\pages\me\me.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular_navigation_nav_params__["a" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_facebook__["a" /* Facebook */],
            __WEBPACK_IMPORTED_MODULE_9__providers_app_app__["a" /* AppProvider */],
            __WEBPACK_IMPORTED_MODULE_8__providers_util_util__["a" /* UtilProvider */]])
    ], MePage);
    return MePage;
}());

//# sourceMappingURL=me.js.map

/***/ }),

/***/ 354:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomestayDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_storage__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__global_global_variable__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_util_util__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_wheel_selector__ = __webpack_require__(215);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var HomestayDetailPage = /** @class */ (function () {
    function HomestayDetailPage(navCtrl, navParams, util, storage, db, camera, selector) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.util = util;
        this.storage = storage;
        this.db = db;
        this.camera = camera;
        this.selector = selector;
        this.user = __WEBPACK_IMPORTED_MODULE_4__global_global_variable__["a" /* GlobalVariables */].user;
        this.homeStay = __WEBPACK_IMPORTED_MODULE_4__global_global_variable__["a" /* GlobalVariables */].homeStay;
        this.pictures = [];
        this.beforeImg = null;
        this.beforeSize = '0';
        this.afterImg = null;
        this.afterSize = '0';
        this.Price = [
            { description: 'Free' },
            { description: '10 USD' },
            { description: '20 USD' },
            { description: '30 USD' },
            { description: '40 USD' },
            { description: '50 USD' },
            { description: '60 USD' },
            { description: '70 USD' },
            { description: '80 USD' },
            { description: '90 USD' },
            { description: '100 USD' },
            { description: '150 USD' },
            { description: '200 USD' },
            { description: '250 USD' },
            { description: '300 USD' },
            { description: '350 USD' },
            { description: '400 USD' },
            { description: '450 USD' },
            { description: '500 USD' },
            { description: '550 USD' },
            { description: '600 USD' },
            { description: '650 USD' },
            { description: '700 USD' },
            { description: '750 USD' },
            { description: '800 USD' },
            { description: '850 USD' },
            { description: '900 USD' },
            { description: '950 USD' },
            { description: '1000 USD' }
        ];
        if (this.navParams.get('homestay')) {
            this.homeStay = this.navParams.get('homestay');
            this.pictures = this.homeStay['pictures'].split('|');
        }
        if (!this.navParams.get('homestay')) {
            this.homeStay['pictures'] = '';
            this.homeStay['breakfast'] = false;
            this.homeStay['wifi'] = false;
        }
    }
    HomestayDetailPage.prototype.ionViewDidLoad = function () {
        // this.itemsCollection = this.db.collection<any>('homestays');
        this.itemsCollection = this.db.collection('users');
    };
    HomestayDetailPage.prototype.submitHomeStay = function () {
        var _this = this;
        var data = { homestay: this.homeStay };
        this.util.presentLoading();
        var userRef = this.itemsCollection.doc(this.user['base64']);
        userRef.update(data).then(function (data) {
            userRef.get().subscribe(function (newdata) {
                __WEBPACK_IMPORTED_MODULE_4__global_global_variable__["a" /* GlobalVariables */].user = newdata.data();
                _this.util.stopLoading();
                _this.navCtrl.pop();
            });
        }, function (err) { _this.util.stopLoading(); });
    };
    //-------------Start pick Price---------------------
    HomestayDetailPage.prototype.openPicker = function () {
        var _this = this;
        this.selector.show({
            title: 'Select Your Price',
            items: [
                this.Price
            ],
            positiveButtonText: 'Done',
            negativeButtonText: 'cancel',
            defaultItems: [
                { index: 0, value: this.Price[0].description }
            ]
        }).then(function (result) {
            console.log(result[0].description);
            _this.homeStay['price'] = result[0].description;
        }, function (err) { return console.log('Error: ', err); });
    };
    //-------------End pick Price---------------------
    //-------------reSize---------------------
    HomestayDetailPage.prototype.uploadFile = function (event) {
        var _this = this;
        var randomName = Math.random().toString(36).substring(2) + Date.now().toString(36);
        var realData = event.split(",")[1];
        var blob = this.b64toBlob(realData, 'image/jpeg');
        var file = this.b64toBlob(realData, 'image/jpeg');
        var filePath = 'uploads/' + randomName + '.jpg';
        var ref = this.storage.ref(filePath);
        var task = ref.put(file);
        this.util.presentLoading();
        task.then(function (res) {
            _this.storage.ref(filePath).getDownloadURL().subscribe(function (data) {
                if (data) {
                    _this.pictures.push(data);
                    _this.homeStay['picture'] = data;
                    _this.homeStay['pictures'] += data + '|';
                }
                _this.util.stopLoading();
            });
        }, function (err) { return _this.util.stopLoading(); });
    };
    HomestayDetailPage.prototype.loadImage = function () {
        var _this = this;
        var options = {
            quality: 100,
            destinationType: this.camera.DestinationType.DATA_URL,
            sourceType: this.camera.PictureSourceType.PHOTOLIBRARY,
            correctOrientation: true,
            allowEdit: false
        };
        this.camera.getPicture(options).then(function (imageData) {
            var base64data = 'data:image/jpeg;base64,' + imageData;
            _this.beforeImg = base64data;
            _this.beforeSize = _this.getImageSize(_this.beforeImg);
            _this.createThumbnail();
        }, function (err) {
            console.log('gallery error: ', err);
        });
    };
    HomestayDetailPage.prototype.createThumbnail = function () {
        var _this = this;
        this.generateFromImage(this.beforeImg, 400, 400, 10, function (data) {
            _this.afterImg = data;
            _this.afterSize = _this.getImageSize(_this.afterImg);
            _this.uploadFile(data);
        });
    };
    HomestayDetailPage.prototype.generateFromImage = function (img, MAX_WIDTH, MAX_HEIGHT, quality, callback) {
        if (MAX_WIDTH === void 0) { MAX_WIDTH = 500; }
        if (MAX_HEIGHT === void 0) { MAX_HEIGHT = 500; }
        if (quality === void 0) { quality = 5; }
        var canvas = document.createElement("canvas");
        var image = new Image();
        image.onload = function () {
            var width = image.width;
            var height = image.height;
            if (width > height) {
                if (width > MAX_WIDTH) {
                    height *= MAX_WIDTH / width;
                    width = MAX_WIDTH;
                }
            }
            else {
                if (height > MAX_HEIGHT) {
                    width *= MAX_HEIGHT / height;
                    height = MAX_HEIGHT;
                }
            }
            canvas.width = width;
            canvas.height = height;
            var ctx = canvas.getContext("2d");
            ctx.drawImage(image, 0, 0, width, height);
            var dataUrl = canvas.toDataURL('image/jpeg', quality);
            callback(dataUrl);
        };
        image.src = img;
    };
    HomestayDetailPage.prototype.getImageSize = function (data_url) {
        var head = 'data:image/jpeg;base64,';
        return ((data_url.length - head.length) * 3 / 4 / (1024 * 1024)).toFixed(4);
    };
    HomestayDetailPage.prototype.b64toBlob = function (b64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 512;
        var byteCharacters = atob(b64Data);
        var byteArrays = [];
        for (var offset = 0; offset < byteCharacters.length; offset += sliceSize) {
            var slice = byteCharacters.slice(offset, offset + sliceSize);
            var byteNumbers = new Array(slice.length);
            for (var i = 0; i < slice.length; i++) {
                byteNumbers[i] = slice.charCodeAt(i);
            }
            var byteArray = new Uint8Array(byteNumbers);
            byteArrays.push(byteArray);
        }
        var blob = new Blob(byteArrays, { type: contentType });
        return blob;
    };
    HomestayDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-homestay-detail',template:/*ion-inline-start:"C:\Users\Kings\Desktop\project08092019\src\pages\homestay-detail\homestay-detail.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Detail</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div class="uploads">\n    <img *ngIf="!homeStay[\'pictures\']" [src]="homeStay[\'picture\']" alt="">\n    <ion-slides *ngIf="homeStay[\'pictures\']" pager="true">\n      <ng-container *ngFor="let pic of pictures">\n        <ion-slide *ngIf="pic.length>0">\n          <img [src]="pic" alt="">\n        </ion-slide>\n      </ng-container>\n    </ion-slides>\n    <div class="upload-btn-wrapper">\n      <button class="btn" (click)="loadImage()">Upload a photo</button>\n    </div>\n  </div>\n  <ion-item>\n    <ion-label floating>Location</ion-label>\n    <ion-input [(ngModel)]="homeStay[\'location\']"></ion-input>\n  </ion-item>\n  <ion-item (click)="openPicker()">\n    <ion-label floating>Daily Price</ion-label>\n    <ion-input [(ngModel)]="homeStay[\'price\']" disabled></ion-input>\n  </ion-item>\n  <ion-item>\n    <ion-label>Wifi</ion-label>\n    <ion-toggle [(ngModel)]="homeStay[\'wifi\']"></ion-toggle>\n  </ion-item>\n  <ion-item>\n    <ion-label>Breakfast</ion-label>\n    <ion-toggle [(ngModel)]="homeStay[\'breakfast\']"></ion-toggle>\n  </ion-item>\n  <ion-item>\n    <ion-label floating>Description</ion-label>\n    <ion-textarea [(ngModel)]="homeStay[\'description\']" rows="4"></ion-textarea>\n  </ion-item>\n  <button [disabled]="!homeStay[\'picture\']" ion-button color="primary" full (tap)="submitHomeStay()">Submit</button>\n</ion-content>'/*ion-inline-end:"C:\Users\Kings\Desktop\project08092019\src\pages\homestay-detail\homestay-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5__providers_util_util__["a" /* UtilProvider */],
            __WEBPACK_IMPORTED_MODULE_2__angular_fire_storage__["a" /* AngularFireStorage */],
            __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_wheel_selector__["a" /* WheelSelector */]])
    ], HomestayDetailPage);
    return HomestayDetailPage;
}());

//# sourceMappingURL=homestay-detail.js.map

/***/ }),

/***/ 357:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SelectCountryPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_util_util__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__global_global_variable__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular_navigation_nav_controller__ = __webpack_require__(44);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_app_app__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var SelectCountryPage = /** @class */ (function () {
    function SelectCountryPage(util, AppPro, navCtrl, zone, db, viewCtrl) {
        this.util = util;
        this.AppPro = AppPro;
        this.navCtrl = navCtrl;
        this.zone = zone;
        this.db = db;
        this.viewCtrl = viewCtrl;
        this.country = '';
        this.user = __WEBPACK_IMPORTED_MODULE_4__global_global_variable__["a" /* GlobalVariables */].user;
        this.countryList = [];
        this.itemsCollection = this.db.collection('users');
        this.getCountryList();
    }
    SelectCountryPage.prototype.onItemSelected = function (ev) {
        this.country = ev;
    };
    SelectCountryPage.prototype.dismiss = function () {
        this.viewCtrl.dismiss();
    };
    SelectCountryPage.prototype.getCountryList = function () {
        var _this = this;
        this.util.presentLoading();
        this.AppPro.getContryList().subscribe(function (data) {
            _this.countryList = data;
            _this.util.stopLoading();
        });
    };
    SelectCountryPage.prototype.save = function () {
        var _this = this;
        if (!this.country) {
            this.util.showAlert('Notice', 'Please select country first');
        }
        else {
            this.user['country'] = this.country;
            __WEBPACK_IMPORTED_MODULE_4__global_global_variable__["a" /* GlobalVariables */].user['country'] = this.country;
            this.util.presentLoading();
            this.itemsCollection.doc(this.user['base64']).update(this.user).then(function () {
                _this.util.stopLoading();
                _this.viewCtrl.dismiss().then(function () {
                    _this.zone.run(function () {
                        _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_tabs_tabs__["a" /* TabsPage */]);
                    });
                });
            });
        }
    };
    SelectCountryPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-select-country',template:/*ion-inline-start:"C:\Users\Kings\Desktop\project08092019\src\pages\select-country\select-country.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-buttons left>\n      <button ion-button clear (tap)="dismiss()">\n        Cancel\n      </button>\n    </ion-buttons>\n    <ion-title text-center>Select Country</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <div class="login-wrap">\n    <h1>Select Your Country</h1>\n\n    <ion-select  [(ngModel)]="country" (ngModelChange)="onItemSelected($event)" placeholder="choose country">\n      <ion-option [value]="country[\'name\']" *ngFor="let item of countryList">{{item[\'name\']}}</ion-option>\n    </ion-select>\n    <button [disabled]="!country" round full color="primary" ion-button icon-left (tap)="save()">\n      Submit\n    </button>\n\n  </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\Kings\Desktop\project08092019\src\pages\select-country\select-country.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__providers_util_util__["a" /* UtilProvider */],
            __WEBPACK_IMPORTED_MODULE_7__providers_app_app__["a" /* AppProvider */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular_navigation_nav_controller__["a" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */]])
    ], SelectCountryPage);
    return SelectCountryPage;
}());

//# sourceMappingURL=select-country.js.map

/***/ }),

/***/ 359:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RoomChatPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_app_app__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_app_chatprovider__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__global_global_variable__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__chats_chats__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_ionic_angular_navigation_nav_params__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_util_util__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pipes_filter_by_filter_by__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__login_login__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__tabs_tabs__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_firebase_app__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_firebase_app__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var RoomChatPage = /** @class */ (function () {
    function RoomChatPage(app, alertCtrl, storage, navCtrl, navParams, util, appProvider, chatProvider) {
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.util = util;
        this.appProvider = appProvider;
        this.chatProvider = chatProvider;
        this.country = '';
        this.language = '';
        this.chatHistory = [];
        this.onlineUsers = [];
        this.countryCount = 0;
        this.chatHistorySearch = [];
    }
    RoomChatPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        var timestamp = __WEBPACK_IMPORTED_MODULE_12_firebase_app__["firestore"].FieldValue.serverTimestamp();
        this.storage.get('checkUser').then(function (val) {
            if (val == true) {
                _this.checkUser = true;
                _this.util.presentLoading();
                _this.appProvider.getOnlineUsers().then(function (data) {
                    _this.onlineUsers = data;
                    _this.chatProvider.getChatList(__WEBPACK_IMPORTED_MODULE_4__global_global_variable__["a" /* GlobalVariables */].user['base64']).subscribe(function (x) {
                        _this.chatHistory = [];
                        x.forEach(function (element) {
                            _this.chatHistory.push(element.payload.doc.data());
                        });
                    });
                    _this.util.stopLoading();
                });
            }
            else {
                _this.checkUser = false;
                _this.showConfirm();
            }
        });
    };
    RoomChatPage.prototype.goToChat = function (user) {
        this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_5__chats_chats__["a" /* ChatsPage */], { receiver: user, user: __WEBPACK_IMPORTED_MODULE_4__global_global_variable__["a" /* GlobalVariables */].user });
    };
    RoomChatPage.prototype.onSearchClearCountry = function () {
        this.country = '';
        this.searchChatHistory();
    };
    RoomChatPage.prototype.onSearchClearLanguage = function () {
        this.language = '';
        this.searchChatHistory();
    };
    RoomChatPage.prototype.searchChatHistory = function () {
        var filterBy = new __WEBPACK_IMPORTED_MODULE_8__pipes_filter_by_filter_by__["a" /* FilterByPipe */]();
        var filter = filterBy.transform(this.chatHistory, {
            country: this.country,
            languages: this.language
        });
        this.chatHistorySearch = filter;
    };
    RoomChatPage.prototype.showConfirm = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Login',
            message: 'Please login to continue !!!',
            buttons: [
                {
                    text: 'Disagree',
                    handler: function () {
                        // this.navCtrl.push(HomePage);
                        _this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_10__tabs_tabs__["a" /* TabsPage */]);
                    }
                },
                {
                    text: 'Agree',
                    handler: function () {
                        _this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_9__login_login__["a" /* LoginPage */]);
                    }
                }
            ]
        });
        confirm.present();
    };
    RoomChatPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-room-chat',template:/*ion-inline-start:"C:\Users\Kings\Desktop\project08092019\src\pages\room-chat\room-chat.html"*/'<ion-header color="color-header">\n  <ion-navbar color="color-header">\n    <ion-title>\n      FACE TRIP\n    </ion-title>\n  </ion-navbar>\n  <div class="searchbox">\n    <ion-searchbar [(ngModel)]="country" (ionClear)="onSearchClearCountry()" (search)="searchChatHistory()"\n      [placeholder]="\'Vietnam\'+\'(\'+countryCount+\')\'" mode="ios"></ion-searchbar>\n    <ion-searchbar [(ngModel)]="language" (ionClear)="onSearchClearLanguage()" (search)="searchChatHistory()"\n      placeholder="Languages" mode="ios"></ion-searchbar>\n    <button ion-button clear icon-only color="light" (tap)="searchChatHistory()">\n      <ion-icon name="md-search"></ion-icon>\n    </button>\n  </div>\n</ion-header>\n<ion-content>\n  <div *ngIf=\'checkUser==true\'>\n    <ion-list>\n      <ion-item (tap)="goToChat(item)" *ngFor="let item of chatHistory">\n        <ion-avatar item-start>\n          <img [src]="item[\'picture\']">\n        </ion-avatar>\n        <h2 *ngIf="item.seen==true; else elseSeen">{{item.name}}</h2>\n        <ng-template #elseSeen>\n          <h2 style=\'font-weight: bold;\'>{{item.name}}</h2>\n        </ng-template>\n        <p>{{item.lastMessage}}</p>\n        <ion-note *ngIf="item.timestamp" item-end>{{item.timestamp.toDate() | date:\'M/d/yyyy H:mm\'}}</ion-note>\n        <!-- <button item-end ion-button round [color]="item[\'online\']? \'secondary\' : \'_gray\'"></button> -->\n      </ion-item>\n    </ion-list>\n  </div>\n</ion-content>'/*ion-inline-end:"C:\Users\Kings\Desktop\project08092019\src\pages\room-chat\room-chat.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_11__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_6_ionic_angular_navigation_nav_params__["a" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_7__providers_util_util__["a" /* UtilProvider */],
            __WEBPACK_IMPORTED_MODULE_2__providers_app_app__["a" /* AppProvider */],
            __WEBPACK_IMPORTED_MODULE_3__providers_app_chatprovider__["a" /* ChatProvider */]])
    ], RoomChatPage);
    return RoomChatPage;
}());

//# sourceMappingURL=room-chat.js.map

/***/ }),

/***/ 360:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return CheckChatOpen; });
var CheckChatOpen = /** @class */ (function () {
    function CheckChatOpen() {
    }
    CheckChatOpen.checkOpen = false;
    return CheckChatOpen;
}());

//# sourceMappingURL=checkopenchat.js.map

/***/ }),

/***/ 361:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__global_global_variable__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_app_app__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_util_util__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular_platform_platform__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__chats_chats__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__local_detail_local_detail__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__popup_popup__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__login_login__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_common_http__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_fire_firestore__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_native_onesignal__ = __webpack_require__(132);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var HomePage = /** @class */ (function () {
    function HomePage(app, alertCtrl, storage, navCtrl, appProvider, util, platform, modalCtrl, http, db, oneSignal) {
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.appProvider = appProvider;
        this.util = util;
        this.platform = platform;
        this.modalCtrl = modalCtrl;
        this.http = http;
        this.db = db;
        this.oneSignal = oneSignal;
        this.country = '';
        this.language = '';
        this.user = __WEBPACK_IMPORTED_MODULE_2__global_global_variable__["a" /* GlobalVariables */].user;
        this.localList = [];
        this.countryCount = 0;
        this.searchCountry = '';
        this.loadCountryJSON();
        this.filtCountry = [];
        this.filtCity = [];
        this.checkClick = true;
        this.checkClickLang = true;
    }
    HomePage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.limituser = 20;
        this.localList = [];
        this.lastVisible = '';
        this.appProvider.getData(this.limituser).then(function (x) {
            x.forEach(function (element) {
                _this.localList.push(element.data());
            });
            _this.lastVisible = x.docs[x.docs.length - 1];
        });
        this.updatePlayerID();
    };
    //++++++++++++++++++++++++++ Start Set user = playerID +++++++++++++++++++++++++++
    HomePage.prototype.updatePlayerID = function () {
        var _this = this;
        if (__WEBPACK_IMPORTED_MODULE_2__global_global_variable__["a" /* GlobalVariables */].user['base64']) {
            this.oneSignal.endInit();
            this.oneSignal.getIds().then(function (id) {
                _this.user['playerID'] = id.userId;
                _this.db.collection('users').doc(_this.user['base64']).update(_this.user).then(function () {
                    __WEBPACK_IMPORTED_MODULE_2__global_global_variable__["a" /* GlobalVariables */].user = _this.user;
                    _this.util.setLocal('user', _this.user);
                });
            });
        }
    };
    //++++++++++++++++++++++++++ End Set user = playerID +++++++++++++++++++++++++++
    HomePage.prototype.filter = function () {
        var _this = this;
        if (this.searchCountry.length >= 3) {
            this.checkClick = false;
            this.filtCountry = this.itemsOnlyCountry.filter(function (item) { return item.country.toLowerCase().indexOf(_this.searchCountry.toLowerCase()) > -1; }).map(function (i) { return i; });
            this.filtCity = this.itemsCountry.filter(function (item) { return (item.city || '').toLowerCase().indexOf(_this.searchCountry.toLowerCase()) > -1; });
        }
        else {
            this.filtCountry = [];
            this.filtCity = [];
            this.checkClick = true;
        }
    };
    HomePage.prototype.filterLang = function () {
        var _this = this;
        if (this.language.length >= 2) {
            this.checkClickLang = false;
            this.filtLang = this.itemsLang.filter(function (item) { return item.lang_name.toLowerCase().indexOf(_this.language.toLowerCase()) > -1; });
        }
        else {
            this.filtLang = [];
            this.checkClickLang = true;
        }
    };
    HomePage.prototype.itemSelect = function (select) {
        this.checkClick = true;
        this.searchCountry = select.country;
        this.searchLocal();
    };
    HomePage.prototype.itemSelectLang = function (select) {
        this.checkClickLang = true;
        this.language = select.lang_name;
        this.searchLocal();
    };
    HomePage.prototype.loadCountryJSON = function () {
        var _this = this;
        this.http
            .get('assets/onlycountry.json')
            .subscribe(function (data) {
            _this.itemsOnlyCountry = data;
        });
        this.http
            .get('assets/country.json')
            .subscribe(function (data) {
            _this.itemsCountry = data;
        });
        this.http
            .get('assets/lang_list.json')
            .subscribe(function (data) {
            _this.itemsLang = data;
        });
    };
    HomePage.prototype.doInfinite = function () {
        var _this = this;
        return new Promise(function (resolve) {
            console.log(resolve);
            setTimeout(function () {
                var userNext = 10;
                _this.limituser += userNext;
                _this.appProvider.getData(_this.limituser).then(function (x) {
                    var next = _this.db.collection('users').ref
                        .where("typeMember", "==", 'true')
                        .orderBy('name')
                        .startAfter(_this.lastVisible)
                        .limit(userNext);
                    next.get().then(function (a) {
                        a.forEach(function (b) {
                            _this.localList.push(b.data());
                            console.log(b.data());
                        });
                    });
                    _this.lastVisible = x.docs[x.docs.length - 1];
                });
                resolve();
            }, 500);
        });
    };
    HomePage.prototype.searchLocal = function () {
        var _this = this;
        try {
            this.util.presentLoading();
            this.countryCount = 0;
            this.appProvider.searchLocal(this.searchCountry, this.language, this.limituser).then(function (res) {
                _this.localList = res;
                _this.util.stopLoading();
            }).catch(function (er) {
            });
        }
        catch (error) {
        }
    };
    HomePage.prototype.onSearchClearCountry = function () {
        var _this = this;
        this.country = '';
        this.util.presentLoading();
        this.appProvider.searchLocal(this.country, this.language, this.limituser).then(function (res) {
            _this.localList = res;
            _this.util.stopLoading();
        });
    };
    HomePage.prototype.onSearchClearLanguage = function () {
        var _this = this;
        this.language = '';
        this.util.presentLoading();
        this.appProvider.searchLocal(this.country, this.language, this.limituser).then(function (res) {
            _this.localList = res;
            _this.util.stopLoading();
        });
    };
    HomePage.prototype.viewDetail = function (user) {
        // GlobalVariables.selectedUser = user;
        // this.events.publish('tab_changed_1');
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_7__local_detail_local_detail__["a" /* LocalDetailPage */], { targetUser: user, myUser: this.user });
        console.log(user);
    };
    HomePage.prototype.openBrowser = function (fb_id) {
        if (this.platform.is('cordova')) {
            cordova.InAppBrowser.open('https://fb.com/' + fb_id, '_blank', 'location=yes');
        }
    };
    HomePage.prototype.askLocals = function () {
        var _this = this;
        var selectedUser = this.localList.filter(function (item) { return item['checked'] == true; });
        var listCheck = [];
        selectedUser.map(function (item) {
            listCheck.push(item);
        });
        this.storage.get('checkUser').then(function (val) {
            if (val == true) {
                if (listCheck.length > 0) {
                    var modal = _this.modalCtrl.create(__WEBPACK_IMPORTED_MODULE_8__popup_popup__["a" /* PopupPage */], { device: listCheck, user: _this.user });
                    modal.present();
                    modal.onDidDismiss(function () {
                        _this.localList.filter(function (item) { return item['checked'] = false; });
                    });
                }
            }
            else {
                _this.showConfirm();
            }
        });
    };
    HomePage.prototype.goToChat = function (user) {
        __WEBPACK_IMPORTED_MODULE_2__global_global_variable__["a" /* GlobalVariables */].selectedUser = user;
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_6__chats_chats__["a" /* ChatsPage */], { receiver: user, user: this.user });
    };
    HomePage.prototype.showConfirm = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Login',
            message: 'Please login to continue !!!',
            buttons: [
                {
                    text: 'Disagree',
                    handler: function () {
                    }
                },
                {
                    text: 'Agree',
                    handler: function () {
                        _this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_10__login_login__["a" /* LoginPage */]);
                    }
                }
            ]
        });
        confirm.present();
    };
    HomePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"C:\Users\Kings\Desktop\project08092019\src\pages\home\home.html"*/'<ion-header>\n  <ion-navbar color="color-header">\n    <ion-title>\n      FACE TRIP\n    </ion-title>\n  </ion-navbar>\n  <div class="searchbox">\n    <ion-searchbar [(ngModel)]="searchCountry" (ionClear)="onSearchClearCountry()" (search)="searchLocal()"\n      [placeholder]="\'country\'+\'(\'+countryCount+\')\'" mode="ios" (ionInput)="filter()"></ion-searchbar>\n    <ion-searchbar [(ngModel)]="language" (ionClear)="onSearchClearLanguage()" (search)="searchLocal()"\n      placeholder="Languages" mode="ios" (ionInput)="filterLang()"></ion-searchbar>\n    <button ion-button clear icon-only color="light" (tap)="searchLocal()">\n      <ion-icon name="md-search"></ion-icon>\n    </button>\n  </div>\n\n  <div class="search-css" *ngIf="searchCountry != \'\' && checkClick == false">\n    <ion-list>\n      <div *ngFor=" let item of filtCountry">\n        <button ion-item *ngIf="filtCountry.length != 0" (click)="itemSelect(item)">\n          <div> {{item.country}}</div>\n        </button>\n      </div>\n      <div *ngFor=" let item of filtCity">\n        <button ion-item (click)="itemSelect(item)">\n          {{item.city}} [{{item.country}}]\n        </button>\n      </div>\n    </ion-list>\n  </div>\n\n  <div class="search-css" *ngIf="language != \'\' && checkClickLang == false">\n    <ion-list>\n      <div *ngFor=" let item of filtLang">\n        <button ion-item *ngIf="filtLang.length != 0" (click)="itemSelectLang(item)">\n          {{item.lang_name}}\n        </button>\n      </div>\n    </ion-list>\n  </div>\n\n</ion-header>\n<ion-content>\n\n\n\n  <button ion-button icon-right (tap)="askLocals()" color="danger">\n    Ask Locals\n    <ion-icon name="md-chatboxes"></ion-icon>\n  </button>\n\n  <div *ngFor="let users of localList">\n    <ion-card\n      *ngIf="users[\'country\'] && (users[\'language1\'] || users[\'language2\'] || users[\'language3\'] || users[\'language4\']) ">\n      <ion-card-content>\n        <ion-grid>\n          <ion-row>\n            <ion-col col-3 text-center>\n              <ion-checkbox [(ngModel)]="users[\'checked\']"></ion-checkbox>\n              <img [src]="users[\'picture\']+\'?type=normal\'" alt="" (tap)="viewDetail(users)">\n            </ion-col>\n\n            <ion-col col-6 (tap)="viewDetail(users)">\n              <ion-grid>\n                <ion-row align-items-start>\n                  <ion-col col-auto>\n                    <b>{{users[\'name\']}}</b>\n                  </ion-col>\n                  <ion-col *ngIf="users[\'guide_fee\'] !== \'Free\' && users[\'guide_fee\'] ; else elseguide" col-auto>\n                    <span>(Fee: ${{users[\'guide_fee\']}})</span></ion-col>\n                  <ng-template #elseguide>\n                    <ion-col col-auto><span>(Fee: Free)</span></ion-col>\n                  </ng-template>\n                </ion-row>\n                <ion-row align-items-center>\n                  <ion-col *ngIf="users[\'city\']; else elsecity" col-auto>\n                    <span class="label">{{users[\'city\']}}, {{users[\'country\']}}</span></ion-col>\n                  <ng-template #elsecity>\n                    <ion-col col-auto><span class="label">{{users[\'country\']}}</span></ion-col>\n                  </ng-template>\n                </ion-row>\n                <ion-row align-items-end>\n                  <ion-col col-auto>\n                    <span class="langs">{{users[\'language1\']}}</span>\n                  </ion-col>\n                  <ion-col *ngIf="users[\'language2\']" col-auto>\n                    <span class="langs">, {{users[\'language2\']}}</span>\n                  </ion-col>\n                  <ion-col *ngIf="users[\'language3\']" col-auto>\n                    <span class="langs">, {{users[\'language3\']}}</span>\n                  </ion-col>\n                  <ion-col *ngIf="users[\'language4\'] " col-auto>\n                    <span class="langs">, {{users[\'language4\']}}</span>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n\n            </ion-col>\n            <ion-col *ngIf="users[\'homestay\']" col-1 text-center>\n              <ion-icon (tap)="viewDetail(users)" color="color-header" name="ios-home" class="ios-home"></ion-icon>\n            </ion-col>\n            <ion-col (tap)="viewDetail(users)" *ngIf="users[\'homestay\']" col-2>\n              <ion-grid>\n                <ion-row align-items-start>\n                  <ion-col *ngIf="users[\'homestay\'][\'price\'] > 0; else elseprice">\n                    <p>${{users[\'homestay\'][\'price\']}}/DAY</p>\n                  </ion-col>\n                  <ng-template #elseprice>\n                    <ion-col>\n                      <P>FREE</P>\n                    </ion-col>\n                  </ng-template>\n                </ion-row>\n                <ion-row align-items-center>\n                  <ion-col *ngIf="users[\'homestay\'][\'wifi\']">\n                    <p>Wifi</p>\n                  </ion-col>\n                </ion-row>\n                <ion-row align-items-end>\n                  <ion-col *ngIf="users[\'homestay\'][\'breakfast\']">\n                    <p>Breakfast</p>\n                  </ion-col>\n                </ion-row>\n              </ion-grid>\n            </ion-col>\n          </ion-row>\n        </ion-grid>\n      </ion-card-content>\n    </ion-card>\n  </div>\n\n  <ion-infinite-scroll (ionInfinite)="$event.waitFor(doInfinite())" threshold="1%">\n    <ion-infinite-scroll-content loadingSpinner="bubbles" loadingText="Loading more data..."> </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Kings\Desktop\project08092019\src\pages\home\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_app_app__["a" /* AppProvider */],
            __WEBPACK_IMPORTED_MODULE_4__providers_util_util__["a" /* UtilProvider */],
            __WEBPACK_IMPORTED_MODULE_5_ionic_angular_platform_platform__["a" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_11__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_12__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_13__ionic_native_onesignal__["a" /* OneSignal */]])
    ], HomePage);
    return HomePage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 362:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LocalDetailPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__edit_profile_edit_profile__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_navigation_nav_params__ = __webpack_require__(24);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__homestay_view_homestay_view__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__chats_chats__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_app_app__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_app_chatprovider__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__login_login__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__tabs_tabs__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_add_operator_toPromise__ = __webpack_require__(635);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_firebase_app__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_firebase_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__ionic_storage__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var LocalDetailPage = /** @class */ (function () {
    function LocalDetailPage(app, alertCtrl, storage, navCtrl, platform, navParams, db, events, 
        //private util: UtilProvider,
        appProvider, modalCtrl, chatProvider) {
        this.app = app;
        this.alertCtrl = alertCtrl;
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.platform = platform;
        this.navParams = navParams;
        this.db = db;
        this.events = events;
        this.appProvider = appProvider;
        this.modalCtrl = modalCtrl;
        this.chatProvider = chatProvider;
        this.homestayList = [];
        this.localList = [];
        this.reviews = [];
        this.comment = '';
        this.pictures = [];
        this.ratingCheck = 0;
        this.countStar = 0;
        this.oneStar = 0;
        this.twoStar = 0;
        this.threeStar = 0;
        this.fourStar = 0;
        this.fiveStar = 0;
        this.user = this.navParams.get('targetUser');
        this.myUser = this.navParams.get('myUser');
        this.loadRating();
        // if (this.navParams.get('user')) {
        //   this.user = this.navParams.get('user');
        //   GlobalVariables.selectedUser = this.user;
        // }
        // this.user = GlobalVariables.selectedUser;
        if (!this.user['pictures']) {
            this.user['pictures'] = '';
        }
        else {
            this.pictures = this.user['pictures'].split('|');
        }
        this.itemsCollection = this.db.collection('users');
    }
    LocalDetailPage.prototype.editProfile = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__edit_profile_edit_profile__["a" /* EditProfilePage */], { user: this.user });
    };
    LocalDetailPage.prototype.viewDetail = function (item) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_5__homestay_view_homestay_view__["a" /* HomestayViewPage */], { homestay: item });
    };
    LocalDetailPage.prototype.goToChat = function () {
        var _this = this;
        this.storage.get('checkUser').then(function (val) {
            if (val == true) {
                // GlobalVariables.selectedUser = this.user;
                // this.events.publish('tab_changed_2');
                // this.navCtrl.setRoot(ContactPage, { receiver: this.user, user: GlobalVariables.user });
                // // this.app.getRootNav().push(ContactPage, { receiver: this.user, user: GlobalVariables.user });
                console.log(_this.user);
                console.log(_this.myUser);
                _this.chatProvider.getDeepChat(_this.myUser, _this.user).get().subscribe(function (x) {
                    var checkRoom = x.data();
                    if (!checkRoom) {
                        var timestamp = __WEBPACK_IMPORTED_MODULE_12_firebase_app__["firestore"].FieldValue.serverTimestamp();
                        var chat1 = { 'lastMessage': '', 'seen': false, 'timestamp': timestamp, 'name': _this.user.name, 'picture': _this.user.picture, 'base64': _this.user.base64 };
                        _this.chatProvider.createRoomChat(_this.myUser, _this.user, chat1);
                        var chat2 = { 'lastMessage': '', 'seen': false, 'timestamp': timestamp, 'name': _this.myUser.name, 'picture': _this.myUser.picture, 'base64': _this.myUser.base64 };
                        _this.chatProvider.createRoomChat(_this.user, _this.myUser, chat2);
                    }
                    _this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_6__chats_chats__["a" /* ChatsPage */], {
                        receiver: _this.user,
                        user: _this.myUser
                    });
                });
            }
            else {
                _this.showConfirm();
            }
        });
    };
    LocalDetailPage.prototype.getPictures = function (strs) {
        return strs.split('|');
    };
    LocalDetailPage.prototype.back = function () {
        this.events.publish('tab_changed_0');
    };
    LocalDetailPage.prototype.openBrowser = function (fb_id) {
        var _this = this;
        this.storage.get('checkUser').then(function (val) {
            if (val == true) {
                if (_this.platform.is('cordova')) {
                    cordova.InAppBrowser.open('https://fb.com/' + fb_id, '_blank', 'location=yes');
                }
            }
            else {
                _this.showConfirm();
            }
        });
    };
    LocalDetailPage.prototype.getHomestayReviews = function () {
        var _this = this;
        this.appProvider.getHomestayReviews(this.homestayList[0]).then(function (data) {
            _this.reviews = data;
        });
    };
    LocalDetailPage.prototype.addReview = function () {
        var _this = this;
        this.storage.get('checkUser').then(function (val) {
            if (val == true) {
                if (_this.ratingCheck > 0) {
                    var currentTimestamp = __WEBPACK_IMPORTED_MODULE_12_firebase_app__["firestore"].Timestamp.fromDate(new Date());
                    var reviewRef = _this.itemsCollection.doc(_this.user['base64']).collection("rating");
                    reviewRef.add({
                        star: _this.ratingCheck,
                        name: _this.myUser['name'],
                        time: currentTimestamp.toDate().toLocaleDateString() + ' ' + currentTimestamp.toDate().toLocaleTimeString(),
                        comment: _this.comment
                    }).then(function () {
                        _this.ratingCheck = 0;
                        _this.comment = '';
                        _this.loadRating();
                    });
                }
            }
            else {
                _this.showConfirm();
            }
        });
    };
    LocalDetailPage.prototype.clickStar = function (number) {
        this.ratingCheck = number;
    };
    LocalDetailPage.prototype.loadRating = function () {
        var _this = this;
        var list = [];
        var ratingRef = this.db.collection("users").doc(this.user['base64']).collection("rating");
        ratingRef.get().toPromise().then(function (doc) {
            doc.forEach(function (doc) {
                list.push(doc.data());
            });
        }).then(function () {
            _this.listComment = list;
        });
    };
    LocalDetailPage.prototype.showConfirm = function () {
        var _this = this;
        var confirm = this.alertCtrl.create({
            title: 'Login',
            message: 'Please login to continue !!!',
            buttons: [
                {
                    text: 'Disagree',
                    handler: function () {
                        // this.navCtrl.push(HomePage);
                        _this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_10__tabs_tabs__["a" /* TabsPage */]);
                    }
                },
                {
                    text: 'Agree',
                    handler: function () {
                        _this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_9__login_login__["a" /* LoginPage */]);
                    }
                }
            ]
        });
        confirm.present();
    };
    LocalDetailPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-local-detail',template:/*ion-inline-start:"C:\Users\Kings\Desktop\project08092019\src\pages\local-detail\local-detail.html"*/'<ion-header>\n  <ion-navbar color="color-header">\n    <!-- <ion-buttons left>\n        <button (tap)="back()" ion-button icon-only clear>\n          <ion-icon name="md-arrow-back"></ion-icon>\n        </button>\n      </ion-buttons> -->\n    <ion-title>\n      FACE TRIP\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content  >\n  <ion-card>\n    <ion-card-content>\n      <ion-grid>\n        <ion-row>\n          <ion-col col-8>\n            <ion-grid>\n              <ion-row>\n                <ion-col>\n                  <b>{{user[\'name\']}}</b>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col *ngIf="user[\'city\']; else elsecity" col-auto>\n                  <span class="label">{{user[\'city\']}}, {{user[\'country\']}}</span>\n                </ion-col>\n                <ng-template #elsecity>\n                  <ion-col col-auto><span class="label">{{user[\'country\']}}</span>\n                  </ion-col>\n                </ng-template>\n              </ion-row>\n              <ion-row align-items-end>\n                <ion-col col-auto>\n                  <span class="label">{{user[\'language1\']}} <ion-badge item-end>{{user[\'lvLanguage1\']}}</ion-badge></span>\n                </ion-col>\n                <ion-col *ngIf="user[\'language2\']" col-auto>\n                  <span class="label">, {{user[\'language2\']}} <ion-badge item-end>{{user[\'lvLanguage2\']}}</ion-badge></span>\n                </ion-col>\n                <ion-col *ngIf="user[\'language3\']" col-auto>\n                  <span class="label">, {{user[\'language3\']}} <ion-badge item-end>{{user[\'lvLanguage3\']}}</ion-badge></span>\n                </ion-col>\n                <ion-col *ngIf="user[\'language4\'] " col-auto>\n                  <span class="label">, {{user[\'language4\']}} <ion-badge item-end>{{user[\'lvLanguage4\']}}</ion-badge></span>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col *ngIf="user[\'guide_fee\'] !== \'Free\' && user[\'guide_fee\']; else elseguide" col-auto>\n                  <span class="label">Fee: ${{user[\'guide_fee\']}}</span>\n                </ion-col>\n                <ng-template #elseguide>\n                  <ion-col col-auto><span class="label">Fee: Free</span>\n                  </ion-col>\n                </ng-template>\n              </ion-row>\n              <ion-row>\n                <ion-col>\n                  <ion-col *ngIf="user[\'meet_at_airport\'] === \'true\'; else elseairport" col-auto>\n                    <span class="label">Meet at the Airport: POSSIBLE</span></ion-col>\n                  <ng-template #elseairport>\n                    <ion-col col-auto>\n                      <span class="label">Meet at the Airport: IMPOSSIBLE</span>\n                    </ion-col>\n                  </ng-template>\n                </ion-col>\n              </ion-row>\n              <ion-row>\n                <ion-col col-auto>\n                  <span class="label">INTEREST: {{user[\'interest\']}}</span>\n                </ion-col>\n              </ion-row>\n            </ion-grid>\n          </ion-col>\n        </ion-row>\n        <ion-row>\n          <ion-col text-right style="justify-content: flex-end">\n            <!-- <button (tap)="openBrowser(user[\'fb_id\'])" color="facebook" ion-button icon-only clear>\n              <ion-icon name="logo-facebook"></ion-icon>\n            </button> -->\n            <button *ngIf="user[\'base64\'] != myUser[\'base64\']; else elseMe" (tap)="goToChat()" color="_gray" ion-button icon-only clear>\n              <ion-icon name="md-chatboxes"></ion-icon>\n            </button>\n            <ng-template #elseMe>\n              <button color="_gray" ion-button icon-only clear>\n                <ion-icon name="md-chatboxes"></ion-icon>\n              </button>\n            </ng-template>\n\n          </ion-col>\n        </ion-row>\n      </ion-grid>\n      <img *ngIf="!user[\'pictures\']" [src]="user[\'picture\']+\'?type=normal\'" alt="">\n      <ion-slides *ngIf="user[\'pictures\']" pager="true">\n        <ng-container *ngFor="let pic of pictures">\n          <ion-slide *ngIf="pic.length>0">\n            <img [src]="pic" alt="">\n          </ion-slide>\n        </ng-container>\n      </ion-slides>\n      <ion-textarea rows="3" [(ngModel)]="user[\'introduction\']" disabled></ion-textarea>\n    </ion-card-content>\n  </ion-card>\n\n  <div class="homestay" *ngIf="user[\'homestay\']">\n    <ion-list>\n      <ion-item-divider>\n        Homestay\n      </ion-item-divider>\n    </ion-list>\n    <div class="uploads">\n      <img *ngIf="!user[\'homestay\'][\'pictures\']" [src]="user[\'homestay\'][\'picture\']" alt="">\n      <ion-slides *ngIf="user[\'homestay\'][\'pictures\']" pager="true">\n        <ng-container *ngFor="let pic of getPictures(user[\'homestay\'][\'pictures\'])">\n          <ion-slide *ngIf="pic.length>0">\n            <img [src]="pic" alt="">\n          </ion-slide>\n        </ng-container>\n      </ion-slides>\n    </div>\n    <ion-grid class="homestay-desc">\n      <ion-row>\n        <ion-col>Locaiton</ion-col>\n        <ion-col>{{user[\'homestay\'][\'location\']}}</ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>Daily Price</ion-col>\n        <ion-col *ngIf="user[\'homestay\'][\'price\'] != \'Free\'; else elseprice">\n          ${{user[\'homestay\'][\'price\']}}\n        </ion-col>\n        <ng-template #elseprice>\n          <ion-col>FREE</ion-col>\n        </ng-template>\n      </ion-row>\n      <ion-row>\n        <ion-col>Wifi</ion-col>\n        <ion-col>{{user[\'homestay\'][\'wifi\']? \'Yes\': \'No\'}}</ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>Breakfast</ion-col>\n        <ion-col>{{user[\'homestay\'][\'breakfast\'] ? \'Yes\': \'No\'}}</ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col>\n          <p>{{user[\'homestay\'][\'description\']}}</p>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n    <p>Reviews</p>\n    <div class="star">\n      <button ion-button icon-only clear (click)="clickStar(1)">\n        <ion-icon name="ios-star-outline" *ngIf="ratingCheck==0" color="star-normal"></ion-icon>\n        <ion-icon name="ios-star" *ngIf="ratingCheck>0"></ion-icon>\n      </button>\n      <button ion-button icon-only clear (click)="clickStar(2)">\n        <ion-icon name="ios-star-outline" *ngIf="ratingCheck<=1" color="star-normal"></ion-icon>\n        <ion-icon name="ios-star" *ngIf="ratingCheck>1"></ion-icon>\n      </button>\n      <button ion-button icon-only clear (click)="clickStar(3)">\n        <ion-icon name="ios-star-outline" *ngIf="ratingCheck<=2" color="star-normal"></ion-icon>\n        <ion-icon name="ios-star" *ngIf="ratingCheck>2"></ion-icon>\n      </button>\n      <button ion-button icon-only clear (click)="clickStar(4)">\n        <ion-icon name="ios-star-outline" *ngIf="ratingCheck<=3" color="star-normal"></ion-icon>\n        <ion-icon name="ios-star" *ngIf="ratingCheck>3"></ion-icon>\n      </button>\n      <button ion-button icon-only clear (click)="clickStar(5)">\n        <ion-icon name="ios-star-outline" *ngIf="ratingCheck<=4" color="star-normal"></ion-icon>\n        <ion-icon name="ios-star" *ngIf="ratingCheck>4"></ion-icon>\n      </button>\n      <button (tap)="addReview()" ion-button small class="submitAddreview">Submit</button>\n      <ion-textarea placeholder="Write your comment for this homestay" [(ngModel)]="comment" rows="3"></ion-textarea>\n    </div>\n\n    <ion-list *ngIf="listComment">\n      <ion-item *ngFor="let review of listComment">\n        <div class="star-show">\n          <h2>{{review.name}}</h2>\n          <button ion-button icon-only clear disabled>\n            <ion-icon name="ios-star-outline" *ngIf="review.star==0"></ion-icon>\n            <ion-icon name="ios-star" *ngIf="review.star>0"></ion-icon>\n          </button>\n          <button ion-button icon-only clear disabled>\n            <ion-icon name="ios-star-outline" *ngIf="review.star<=1"></ion-icon>\n            <ion-icon name="ios-star" *ngIf="review.star>1"></ion-icon>\n          </button>\n          <button ion-button icon-only clear disabled>\n            <ion-icon name="ios-star-outline" *ngIf="review.star<=2"></ion-icon>\n            <ion-icon name="ios-star" *ngIf="review.star>2"></ion-icon>\n          </button>\n          <button ion-button icon-only clear disabled>\n            <ion-icon name="ios-star-outline" *ngIf="review.star<=3"></ion-icon>\n            <ion-icon name="ios-star" *ngIf="review.star>3"></ion-icon>\n          </button>\n          <button ion-button icon-only clear disabled>\n            <ion-icon name="ios-star-outline" *ngIf="review.star<=4"></ion-icon>\n            <ion-icon name="ios-star" *ngIf="review.star>4"></ion-icon>\n          </button>\n        </div>\n        <p>{{review.comment}}</p>\n        <p>{{review.time}}</p>\n      </ion-item>\n    </ion-list>\n\n</ion-content>'/*ion-inline-end:"C:\Users\Kings\Desktop\project08092019\src\pages\local-detail\local-detail.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_13__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular_navigation_nav_params__["a" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_7__providers_app_app__["a" /* AppProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_8__providers_app_chatprovider__["a" /* ChatProvider */]])
    ], LocalDetailPage);
    return LocalDetailPage;
}());

//# sourceMappingURL=local-detail.js.map

/***/ }),

/***/ 363:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return HomestayViewPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_storage__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__global_global_variable__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_util_util__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_app_app__ = __webpack_require__(43);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var HomestayViewPage = /** @class */ (function () {
    function HomestayViewPage(navCtrl, navParams, util, events, appProvider, storage, db) {
        var _this = this;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.util = util;
        this.events = events;
        this.appProvider = appProvider;
        this.storage = storage;
        this.db = db;
        this.rating = 4;
        this.user = __WEBPACK_IMPORTED_MODULE_4__global_global_variable__["a" /* GlobalVariables */].user;
        this.homeStay = {};
        this.reviews = [];
        this.comment = '';
        this.pictures = [];
        events.subscribe('star-rating:changed', function (starRating) {
            console.log(starRating);
            _this.rating = starRating;
        });
        this.homeStay = Object.assign({}, this.navParams.get('homestay'));
        if (this.homeStay['pictures']) {
            this.pictures = this.homeStay['pictures'].split('|');
        }
    }
    HomestayViewPage.prototype.uploadFile = function (event) {
        var _this = this;
        var file = event.target.files[0];
        var filePath = 'uploads/' + file['name'];
        var ref = this.storage.ref(filePath);
        var task = ref.put(file);
        task.then(function (res) {
            _this.storage.ref(filePath).getDownloadURL().subscribe(function (data) {
                if (data) {
                    _this.homeStay['picture'] = data;
                }
            });
        });
    };
    HomestayViewPage.prototype.ionViewDidLoad = function () {
        this.itemsCollection = this.db.collection('homestays');
        console.log('ionViewDidLoad HomestayDetailPage');
        this.getHomestayReviews();
    };
    HomestayViewPage.prototype.submitHomeStay = function () {
        var _this = this;
        this.util.presentLoading();
        var userRef = this.itemsCollection.doc(this.user['base64']);
        if (this.homeStay['id']) {
            userRef.collection("items").doc(this.homeStay['id']).update(this.homeStay).then(function (data) {
                console.log(data);
                _this.util.stopLoading();
            }, function (err) { _this.util.stopLoading(); });
        }
        else {
            userRef.collection("items").add(this.homeStay).then(function (data) {
                console.log(data);
                _this.util.stopLoading();
            }, function (err) { _this.util.stopLoading(); });
        }
    };
    HomestayViewPage.prototype.getHomestayReviews = function () {
        var _this = this;
        this.appProvider.getHomestayReviews(this.homeStay).then(function (data) {
            _this.reviews = data;
        });
    };
    HomestayViewPage.prototype.addReview = function () {
        var _this = this;
        if (this.comment != '') {
            this.util.presentLoading();
            this.appProvider.addReview(this.user, this.homeStay, this.comment).then(function () {
                _this.util.stopLoading();
                _this.getHomestayReviews();
            });
        }
        else {
            this.util.showAlert('Notice', 'Please input your review first!');
        }
    };
    HomestayViewPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-homestay-view',template:/*ion-inline-start:"C:\Users\Kings\Desktop\project08092019\src\pages\homestay-view\homestay-view.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>Detail</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content padding>\n  <div class="uploads">\n    <img *ngIf="!homeStay[\'pictures\']" [src]="homeStay[\'picture\']" alt="">\n    <ion-slides *ngIf="homeStay[\'pictures\']">\n      <ng-container *ngFor="let pic of pictures">\n        <ion-slide>\n          <img [src]="pic" alt="">\n        </ion-slide>\n      </ng-container>\n\n    </ion-slides>\n  </div>\n  <ion-grid class="homestay-desc">\n    <ion-row>\n      <ion-col>Locaiton</ion-col>\n      <ion-col>{{homeStay[\'location\']}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>Daily Price</ion-col>\n      <ion-col>{{homeStay[\'price\']|currency:\'USD\'}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>Wifi</ion-col>\n      <ion-col>{{homeStay[\'wifi\']? \'Yes\': \'No\'}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>Breakfast</ion-col>\n      <ion-col>{{homeStay[\'breakfast\'] ? \'Yes\': \'No\'}}</ion-col>\n    </ion-row>\n    <ion-row>\n      <ion-col>\n        <p>{{homeStay[\'description\']}}</p>\n      </ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-item-divider>Reviews</ion-item-divider>\n  <ion-list>\n    <ion-item *ngFor="let review of reviews;">\n      <ion-avatar item-start>\n        <img [src]="review[\'picture\']">\n      </ion-avatar>\n      <h2>{{review[\'name\']}}</h2>\n      <p>{{review[\'review\']}}</p>\n    </ion-item>\n  </ion-list>\n  <p>Write your Review</p>\n  <ion-textarea placeholder="Write your comment for this homestay" [(ngModel)]="comment" rows="4"></ion-textarea>\n  <button (tap)="addReview()" ion-button small>Submit</button>\n  <br>\n</ion-content>\n'/*ion-inline-end:"C:\Users\Kings\Desktop\project08092019\src\pages\homestay-view\homestay-view.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5__providers_util_util__["a" /* UtilProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["d" /* Events */],
            __WEBPACK_IMPORTED_MODULE_6__providers_app_app__["a" /* AppProvider */],
            __WEBPACK_IMPORTED_MODULE_2__angular_fire_storage__["a" /* AngularFireStorage */],
            __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], HomestayViewPage);
    return HomestayViewPage;
}());

//# sourceMappingURL=homestay-view.js.map

/***/ }),

/***/ 364:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PopupPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_app_app__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_app_chatprovider__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase_app__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_firebase_app__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




//import { UtilProvider } from '../../providers/util/util';



var PopupPage = /** @class */ (function () {
    function PopupPage(navCtrl, navParams, viewCtrl, db, https, 
        // private util: UtilProvider,
        appProvider, toastCtrl, chatProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.viewCtrl = viewCtrl;
        this.db = db;
        this.https = https;
        this.appProvider = appProvider;
        this.toastCtrl = toastCtrl;
        this.chatProvider = chatProvider;
        this.message = '';
        this.receiver = {};
        this.myUser = {};
        this.msgList = [];
        this.chatCollections = this.db.collection('Chats');
    }
    PopupPage.prototype.ionViewWillEnter = function () {
        var timestamp = __WEBPACK_IMPORTED_MODULE_6_firebase_app__["firestore"].FieldValue.serverTimestamp();
        this.listUser = this.navParams.get('device');
        this.myUser = this.navParams.get('user');
        this.messData = {
            'base64': this.myUser['base64'],
            'text': this.message,
            'timestamp': timestamp
        };
    };
    PopupPage.prototype.clickSend = function () {
        if (this.message != '') {
            var i = 0;
            while (i < this.listUser.length) {
                this.createRoom(this.myUser, this.listUser[i], this.message);
                i += 1;
            }
            this.message = '';
            this.presentToast('successfully');
            this.viewCtrl.dismiss();
        }
    };
    PopupPage.prototype.createRoom = function (myUser, listUser, Mess) {
        var _this = this;
        var dataReceiver;
        this.chatProvider.getUserChat(listUser['base64']).get().subscribe(function (data) {
            dataReceiver = data.data().playerID;
        });
        this.chatProvider.getDeepChat(myUser, listUser).get().subscribe(function (x) {
            var checkRoom = x.data();
            var timestamp = __WEBPACK_IMPORTED_MODULE_6_firebase_app__["firestore"].FieldValue.serverTimestamp();
            var dataMess;
            if (!checkRoom) {
                var chat1 = { 'lastMessage': Mess, 'seen': false, 'timestamp': timestamp, 'name': listUser.name, 'picture': listUser.picture, 'base64': listUser.base64 };
                _this.chatProvider.createRoomChat(myUser, listUser, chat1);
                var chat2 = { 'lastMessage': Mess, 'seen': false, 'timestamp': timestamp, 'name': myUser.name, 'picture': myUser.picture, 'base64': myUser.base64 };
                _this.chatProvider.createRoomChat(listUser, myUser, chat2);
            }
            else {
                _this.chat1 = _this.chatProvider.getDeepChat(myUser, listUser);
                _this.chat2 = _this.chatProvider.getDeepChat(listUser, myUser);
                _this.chat1
                    .update({
                    lastMessage: Mess,
                    timestamp: timestamp
                });
                _this.chat2
                    .update({
                    lastMessage: Mess,
                    timestamp: timestamp,
                    seen: false
                });
            }
            dataMess = _this.chatProvider.getMessages(listUser['base64'], myUser['base64']);
            dataMess.get().subscribe(function (data) {
                if (data.size === 0) {
                    dataMess = _this.chatProvider.getMessages(myUser['base64'], listUser['base64']);
                    _this.sendMess(dataMess, Mess, myUser, dataReceiver);
                }
                else {
                    _this.sendMess(dataMess, Mess, myUser, dataReceiver);
                }
            });
        });
    };
    PopupPage.prototype.sendMess = function (dataMess, message, myUser, dataReceiver) {
        var _this = this;
        var timestamp = __WEBPACK_IMPORTED_MODULE_6_firebase_app__["firestore"].FieldValue.serverTimestamp();
        var messData = {
            'base64': this.myUser['base64'],
            'text': message,
            'timestamp': timestamp
        };
        this.chatProvider.createMess(messData, dataMess).then(function () {
            if (dataReceiver) {
                _this.sendNoti(message, myUser, dataReceiver);
            }
        });
    };
    PopupPage.prototype.sendNoti = function (message, idSend, receive) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_4__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var data = JSON.stringify({
            app_id: "54940e0d-ca0d-4087-8e3c-57a70c54a20e",
            include_player_ids: [receive],
            headings: { en: "You have a message from " + idSend['name'] },
            contents: { en: message },
            data: { dataReceiver: this.myUser },
        });
        return new Promise(function (resolve, reject) {
            _this.https.post('https://onesignal.com/api/v1/notifications', data, options)
                .toPromise()
                .then(function (response) {
                resolve(response.json());
            })
                .catch(function (error) {
                console.error('API Error : ', error.status);
                console.error('API Error : ', JSON.stringify(error));
                reject(error.json());
            });
        });
    };
    PopupPage.prototype.clickClose = function () {
        this.viewCtrl.dismiss();
    };
    PopupPage.prototype.presentToast = function (data) {
        var toast = this.toastCtrl.create({
            message: data,
            duration: 3000,
            position: 'middle'
        });
        toast.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], PopupPage.prototype, "content", void 0);
    PopupPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-popup',template:/*ion-inline-start:"C:\Users\Kings\Desktop\project08092019\src\pages\popup\popup.html"*/'<ion-content>\n  <div class="conten">\n    <h4>Message</h4>\n    <!-- <h2>Message</h2> -->\n    <div *ngFor="let user of listUser; let i = index">\n      <h5 *ngIf="listUser.length == 1">To: {{user.name}}</h5>\n      <h5 *ngIf="i==0 && listUser.length > 1 ">To: {{user.name}} and {{listUser.length-1}} users</h5>\n    </div>\n    <div class="bt-popup">\n      <ion-textarea rows="2" [(ngModel)]="message" placeholder="Message"></ion-textarea>\n      <div class="send-close">\n        <button ion-button color="primary" (click)="clickSend()">SEND</button>\n        <button ion-button color="primary" (click)="clickClose()">CLOSE</button>\n      </div>\n    </div>\n  </div>\n</ion-content>'/*ion-inline-end:"C:\Users\Kings\Desktop\project08092019\src\pages\popup\popup.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_4__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_2__providers_app_app__["a" /* AppProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* ToastController */],
            __WEBPACK_IMPORTED_MODULE_3__providers_app_chatprovider__["a" /* ChatProvider */]])
    ], PopupPage);
    return PopupPage;
}());

//# sourceMappingURL=popup.js.map

/***/ }),

/***/ 365:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(366);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(488);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_http__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pipes_filter_by_filter_by__ = __webpack_require__(216);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__util_util__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_operators__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_rxjs_add_operator_map__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






//import { GlobalVariables } from '../../global/global_variable';
//import { AutoCompleteService } from 'ionic2-auto-complete';


var AppProvider = /** @class */ (function () {
    function AppProvider(http, db, https, util) {
        this.http = http;
        this.db = db;
        this.https = https;
        this.util = util;
        this.messages = [];
        this.listeners = [];
        this.start = null;
        this.end = null;
        this.localCollections = this.db.collection('users');
        this.homestayCollections = this.db.collection('homestays');
        this.chatCollections = this.db.collection('chats');
        this.chatHistoryCollections = this.db.collection('chatHistory');
        this.reviewCollections = this.db.collection('reviews');
    }
    AppProvider.prototype.getResults = function (keyword) {
        return this.http.get("https://restcountries.eu/rest/v1/name/" + keyword).pipe(Object(__WEBPACK_IMPORTED_MODULE_6_rxjs_operators__["map"])(function (result) {
            return result.filter(function (item) { return item.name.toLowerCase().startsWith(keyword.toLowerCase()); }).map(function (i) { return i.name; });
        }));
    };
    AppProvider.prototype.getContryList = function () {
        return this.http.get('https://restcountries.eu/rest/v2/all');
    };
    AppProvider.prototype.setPlayerID = function () {
    };
    AppProvider.prototype.setPlayerIDNull = function (idUser) {
        return this.db.collection('users').doc(idUser).update({ playerID: null });
    };
    AppProvider.prototype.searchLocal = function (country, language, numberLimit) {
        var _this = this;
        return new Promise(function (resolve) {
            var rs = [];
            var filterBy = new __WEBPACK_IMPORTED_MODULE_4__pipes_filter_by_filter_by__["a" /* FilterByPipe */]();
            _this.localCollections.ref.limit(numberLimit).orderBy('name').get().then(function (snap) {
                snap.forEach(function (doc) {
                    var docData = doc.data();
                    docData['country'] = docData['country'] || '';
                    docData['languages'] = docData['language1'] + "," + docData['language2'] + "," + docData['language3'] + "," + docData['language4'];
                    rs.push(docData);
                });
                var filter = filterBy.transform(rs, {
                    country: country,
                    languages: language
                });
                resolve(filter);
            }).catch(function (err) { return console.log(err); });
        }).catch(function (err) {
        });
    };
    AppProvider.prototype.getMess = function (id1, id2, numberLimit) {
        var user = this.db.collection('chats').doc(this.util.getChatId(id1, id2)).collection('chats');
        var firstMess = user.ref
            .orderBy('date', "desc")
            .limit(numberLimit);
        return firstMess.get();
        // return this.db.collection('chats').doc(this.util.getChatId(id1, id2)).collection('chats', ref => ref.orderBy('date').limit(numberLimit)).snapshotChanges().map((actions)=>{
        //   return actions.map((snapshot) => {
        //     const data = snapshot.payload.doc.data();
        //     data.id = snapshot.payload.doc.id;
        //     return data;
        //   });
        // });
    };
    AppProvider.prototype.getData = function (numberLimit) {
        var user = this.db.collection('users');
        var firstPage = user.ref
            .where("typeMember", "==", 'true')
            .orderBy('name')
            .limit(numberLimit);
        return firstPage.get();
    };
    AppProvider.prototype.addReview = function (user, homestay, review) {
        return this.reviewCollections.doc(homestay['id']).collection('items').add({
            name: user['name'],
            picture: user['picture'],
            review: review
        });
    };
    AppProvider.prototype.getHomestayReviews = function (homestay) {
        var _this = this;
        return new Promise(function (resolve) {
            var rs = [];
            _this.reviewCollections.doc(homestay['id']).collection('items').get().subscribe(function (snap) {
                snap.forEach(function (doc) {
                    var docData = doc.data();
                    docData['id'] = doc.id;
                    rs.push(docData);
                });
                resolve(rs);
            }, function (err) { return resolve([]); });
        });
    };
    AppProvider.prototype.getChatList = function (id1, id2) {
        var _this = this;
        var chatId = '';
        if (id1 < id2) {
            chatId = id1 + id2;
        }
        else {
            chatId = id2 + id1;
        }
        return new Promise(function (resolve) {
            var rs = [];
            _this.chatCollections.doc(chatId).collection('items', function (ref) { return ref.orderBy('date'); }).get().subscribe(function (snap) {
                snap.forEach(function (doc) {
                    var docData = doc.data();
                    docData['id'] = doc.id;
                    rs.push(docData);
                });
                resolve(rs);
            }, function (err) { return resolve(rs); });
        });
    };
    AppProvider.prototype.addChageHistory = function (userId, partner) {
        return this.chatHistoryCollections.doc(userId['base64']).collection('items').doc(partner['base64']).set(partner);
    };
    AppProvider.prototype.getChatHistory = function (userID) {
        var _this = this;
        return new Promise(function (resolve) {
            var rs = [];
            _this.chatHistoryCollections.doc(userID).collection('items').get().subscribe(function (snap) {
                snap.forEach(function (doc) {
                    var docData = doc.data();
                    docData['id'] = doc.id;
                    rs.push(docData);
                });
                resolve(rs);
            }, function (err) { return resolve(); });
        });
    };
    AppProvider.prototype.submitChat = function (id1, id2, message, myInfo, targetid) {
        var _this = this;
        var chatId = '';
        if (id1 < id2) {
            chatId = id1 + id2;
        }
        else {
            chatId = id2 + id1;
        }
        return this.chatCollections.doc(chatId).collection('chats').add({
            sender: id1,
            receiver: id2,
            message: message,
            date: new Date().getTime()
        }).then(function () {
            if (targetid) {
                var headers = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["a" /* Headers */]({
                    'Content-Type': 'application/json'
                });
                var options_1 = new __WEBPACK_IMPORTED_MODULE_1__angular_http__["d" /* RequestOptions */]({ headers: headers });
                var data_1 = JSON.stringify({
                    app_id: "54940e0d-ca0d-4087-8e3c-57a70c54a20e",
                    include_player_ids: [targetid],
                    headings: { en: "You have a message from " + myInfo['name'] },
                    contents: { en: message },
                    data: { dataReceiver: myInfo },
                });
                return new Promise(function (resolve, reject) {
                    _this.https.post('https://onesignal.com/api/v1/notifications', data_1, options_1)
                        .toPromise()
                        .then(function (response) {
                        console.log('API Response : ', response.json());
                        resolve(response.json());
                    })
                        .catch(function (error) {
                        console.error('API Error : ', error.status);
                        console.error('API Error : ', JSON.stringify(error));
                        reject(error.json());
                    });
                });
            }
        });
    };
    AppProvider.prototype.getOnlineUsers = function () {
        var _this = this;
        return new Promise(function (resolve) {
            var rs = [];
            _this.localCollections.get().subscribe(function (snap) {
                snap.forEach(function (doc) {
                    var docData = doc.data();
                    docData['id'] = doc.id;
                    if (docData['online'] == true) {
                        rs.push(docData);
                    }
                });
                resolve(rs);
            }, function (err) { return resolve(); });
        });
    };
    AppProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_2__angular_core__["A" /* Injectable */])()
        /*export class AppProvider implements AutoCompleteService {*/
        ,
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_3__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_1__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_5__util_util__["a" /* UtilProvider */]])
    ], AppProvider);
    return AppProvider;
}());

//# sourceMappingURL=app.js.map

/***/ }),

/***/ 47:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return GlobalVariables; });
var GlobalVariables = /** @class */ (function () {
    function GlobalVariables() {
    }
    GlobalVariables.user = {};
    GlobalVariables.homeStay = {
        breakfast: '',
        description: '',
        location: '',
        picture: '',
        pictures: '',
        price: '',
        wifi: ''
    };
    GlobalVariables.selectedUser = {};
    GlobalVariables.countries = [];
    return GlobalVariables;
}());

//# sourceMappingURL=global_variable.js.map

/***/ }),

/***/ 488:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export firebaseConfig */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(58);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(529);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_http__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_onesignal__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_util_util__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__angular_common_http__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__angular_fire__ = __webpack_require__(70);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__angular_fire_auth__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__angular_fire_firestore__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_app_app__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_app_chatprovider__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__angular_fire_storage__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pipes_pipes_module__ = __webpack_require__(637);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__components_components_module__ = __webpack_require__(639);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__ionic_native_facebook__ = __webpack_require__(358);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_native_camera__ = __webpack_require__(214);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_wheel_selector__ = __webpack_require__(215);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__pages_me_me__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_23__pages_room_chat_room_chat__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_24__pages_home_home__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_25__pages_tabs_tabs__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_26__pages_login_login__ = __webpack_require__(93);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_27__pages_edit_profile_edit_profile__ = __webpack_require__(198);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_28__pages_homestay_detail_homestay_detail__ = __webpack_require__(354);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_29__pages_local_detail_local_detail__ = __webpack_require__(362);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_30__pages_homestay_view_homestay_view__ = __webpack_require__(363);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_31__pages_chats_chats__ = __webpack_require__(96);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_32__pages_popup_popup__ = __webpack_require__(364);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_33__pages_select_country_select_country__ = __webpack_require__(357);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


































var firebaseConfig = {
    apiKey: "AIzaSyBvEJmRrEhzkoirQaTTJEYLnVLVZZ6urHM",
    authDomain: "facetriper.firebaseio.com",
    databaseURL: "https://facetriper.firebaseio.com",
    projectId: "facetriper",
    storageBucket: "facetriper.appspot.com",
    messagingSenderId: "443594649686"
};
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_22__pages_me_me__["a" /* MePage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_room_chat_room_chat__["a" /* RoomChatPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_edit_profile_edit_profile__["a" /* EditProfilePage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_homestay_detail_homestay_detail__["a" /* HomestayDetailPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_local_detail_local_detail__["a" /* LocalDetailPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_homestay_view_homestay_view__["a" /* HomestayViewPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_chats_chats__["a" /* ChatsPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_popup_popup__["a" /* PopupPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_select_country_select_country__["a" /* SelectCountryPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_10__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_4__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {
                    tabsHideOnSubPages: false,
                }, {
                    links: []
                }),
                __WEBPACK_IMPORTED_MODULE_9__ionic_storage__["a" /* IonicStorageModule */].forRoot({
                    name: '_facetrip',
                    driverOrder: ['indexeddb', 'sqlite', 'websql'],
                }),
                __WEBPACK_IMPORTED_MODULE_11__angular_fire__["a" /* AngularFireModule */].initializeApp(firebaseConfig),
                __WEBPACK_IMPORTED_MODULE_13__angular_fire_firestore__["b" /* AngularFirestoreModule */],
                __WEBPACK_IMPORTED_MODULE_12__angular_fire_auth__["b" /* AngularFireAuthModule */],
                __WEBPACK_IMPORTED_MODULE_16__angular_fire_storage__["b" /* AngularFireStorageModule */],
                __WEBPACK_IMPORTED_MODULE_17__pipes_pipes_module__["a" /* PipesModule */],
                __WEBPACK_IMPORTED_MODULE_18__components_components_module__["a" /* ComponentsModule */]
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_22__pages_me_me__["a" /* MePage */],
                __WEBPACK_IMPORTED_MODULE_23__pages_room_chat_room_chat__["a" /* RoomChatPage */],
                __WEBPACK_IMPORTED_MODULE_24__pages_home_home__["a" /* HomePage */],
                __WEBPACK_IMPORTED_MODULE_25__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_26__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_27__pages_edit_profile_edit_profile__["a" /* EditProfilePage */],
                __WEBPACK_IMPORTED_MODULE_28__pages_homestay_detail_homestay_detail__["a" /* HomestayDetailPage */],
                __WEBPACK_IMPORTED_MODULE_29__pages_local_detail_local_detail__["a" /* LocalDetailPage */],
                __WEBPACK_IMPORTED_MODULE_30__pages_homestay_view_homestay_view__["a" /* HomestayViewPage */],
                __WEBPACK_IMPORTED_MODULE_31__pages_chats_chats__["a" /* ChatsPage */],
                __WEBPACK_IMPORTED_MODULE_32__pages_popup_popup__["a" /* PopupPage */],
                __WEBPACK_IMPORTED_MODULE_33__pages_select_country_select_country__["a" /* SelectCountryPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_6__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_7__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_8__providers_util_util__["a" /* UtilProvider */],
                __WEBPACK_IMPORTED_MODULE_14__providers_app_app__["a" /* AppProvider */],
                __WEBPACK_IMPORTED_MODULE_15__providers_app_chatprovider__["a" /* ChatProvider */],
                __WEBPACK_IMPORTED_MODULE_19__ionic_native_facebook__["a" /* Facebook */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_wheel_selector__["a" /* WheelSelector */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_onesignal__["a" /* OneSignal */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 529:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(329);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_app_app__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__global_global_variable__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__global_checkopenchat__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__angular_fire_firestore__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__providers_util_util__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_native_onesignal__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__ionic_storage__ = __webpack_require__(56);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_chats_chats__ = __webpack_require__(96);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};













var MyApp = /** @class */ (function () {
    function MyApp(storage, platform, statusBar, splashScreen, 
        // private toastCtrl: ToastController,
        app, oneSignal, util, appProvider, db) {
        var _this = this;
        this.storage = storage;
        this.app = app;
        this.util = util;
        this.appProvider = appProvider;
        this.db = db;
        this.countryList = [];
        this.signal_app_id = '54940e0d-ca0d-4087-8e3c-57a70c54a20e';
        this.firebase_id = '443594649686';
        platform.ready().then(function () {
            statusBar.styleDefault();
            splashScreen.hide();
            _this.getCountryList().then(function () { });
            _this.itemsCollections = _this.db.collection('users');
            _this.util.getLocal('user').then(function (user) {
                if (user) {
                    _this.itemsCollections.doc(btoa(user['email'])).get().subscribe(function (snap) {
                        if (snap.exists) {
                            __WEBPACK_IMPORTED_MODULE_6__global_global_variable__["a" /* GlobalVariables */].user = snap.data();
                            var usr = snap.data();
                            usr['online'] = true;
                            _this.itemsCollections.doc(__WEBPACK_IMPORTED_MODULE_6__global_global_variable__["a" /* GlobalVariables */].user['base64']).update(usr).then(function () {
                            });
                            storage.set('checkUser', true);
                            _this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
                        }
                        else {
                            storage.set('checkUser', false);
                            _this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
                        }
                    });
                }
                else {
                    storage.set('checkUser', false);
                    _this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_tabs_tabs__["a" /* TabsPage */];
                }
            });
            /*++++++++++++++++++++++++++ Start Notification +++++++++++++++++++++++++++*/
            oneSignal.startInit(_this.signal_app_id, _this.firebase_id);
            oneSignal.inFocusDisplaying(oneSignal.OSInFocusDisplayOption.Notification);
            oneSignal.handleNotificationOpened().subscribe(function (jsonData) {
                if (__WEBPACK_IMPORTED_MODULE_7__global_checkopenchat__["a" /* CheckChatOpen */].checkOpen == true) {
                    _this.app.goBack();
                }
                setTimeout(function () {
                    _this.app.getRootNav().push(__WEBPACK_IMPORTED_MODULE_12__pages_chats_chats__["a" /* ChatsPage */], {
                        receiver: jsonData.notification.payload.additionalData.dataReceiver,
                        user: __WEBPACK_IMPORTED_MODULE_6__global_global_variable__["a" /* GlobalVariables */].user
                    });
                    oneSignal.clearOneSignalNotifications();
                    _this.checkNoti = jsonData.notification.payload.title;
                }, 3000);
            });
            oneSignal.handleNotificationReceived().subscribe(function (res) {
                var checkMe = 'You have a message from ' + __WEBPACK_IMPORTED_MODULE_7__global_checkopenchat__["a" /* CheckChatOpen */].checkNoti;
                if (__WEBPACK_IMPORTED_MODULE_7__global_checkopenchat__["a" /* CheckChatOpen */].checkOpen == true && res.payload.title === checkMe) {
                    oneSignal.clearOneSignalNotifications();
                }
                // let toast = this.toastCtrl.create({
                //   message: res.payload.title,
                //   duration: 3000,
                //   position: 'top'
                // });
                // toast.present();
            });
            oneSignal.endInit();
            /*++++++++++++++++++++++++++ End Notification +++++++++++++++++++++++++++++*/
        });
    }
    MyApp.prototype.getCountryList = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.appProvider.getContryList().subscribe(function (data) {
                if (data) {
                    _this.countryList = data;
                    __WEBPACK_IMPORTED_MODULE_6__global_global_variable__["a" /* GlobalVariables */].countries = data;
                }
                resolve();
            }, function (err) { return resolve(); });
        });
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\Kings\Desktop\project08092019\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"C:\Users\Kings\Desktop\project08092019\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_11__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_10__ionic_native_onesignal__["a" /* OneSignal */],
            __WEBPACK_IMPORTED_MODULE_9__providers_util_util__["a" /* UtilProvider */],
            __WEBPACK_IMPORTED_MODULE_5__providers_app_app__["a" /* AppProvider */], __WEBPACK_IMPORTED_MODULE_8__angular_fire_firestore__["a" /* AngularFirestore */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 61:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__me_me__ = __webpack_require__(332);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__room_chat_room_chat__ = __webpack_require__(359);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__home_home__ = __webpack_require__(361);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular__ = __webpack_require__(16);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = /** @class */ (function () {
    function TabsPage(events) {
        this.events = events;
        this.tab1Root = __WEBPACK_IMPORTED_MODULE_3__home_home__["a" /* HomePage */];
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_1__me_me__["a" /* MePage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__room_chat_room_chat__["a" /* RoomChatPage */];
    }
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["l" /* Tabs */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_4_ionic_angular__["l" /* Tabs */])
    ], TabsPage.prototype, "tabs", void 0);
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"C:\Users\Kings\Desktop\project08092019\src\pages\tabs\tabs.html"*/'<ion-tabs color="xam" tabsLayout="icon-end" selectedIndex=\'0\'>\n  <ion-tab [root]="tab1Root" tabTitle="LOCALS" ></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="ME" ></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="CHAT" tabIcon="ios-chatbubbles-outline" class="tabs-icon-top tabs-positive"></ion-tab>\n</ion-tabs>\n'/*ion-inline-end:"C:\Users\Kings\Desktop\project08092019\src\pages\tabs\tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_4_ionic_angular__["d" /* Events */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ }),

/***/ 634:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaseService; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_map__);
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};



var extractError = function (error) {
    var errMsg;
    if (error instanceof __WEBPACK_IMPORTED_MODULE_0__angular_http__["e" /* Response */]) {
        var body = error.json() || '';
        var err = body.error || JSON.stringify(body);
        errMsg = error.status + " - " + (error.statusText || '') + " " + err;
    }
    else {
        errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return errMsg;
};
var BaseService = /** @class */ (function () {
    function BaseService() {
    }
    BaseService.prototype.handlePromiseError = function (error) {
        return Promise.reject(extractError(error));
    };
    BaseService.prototype.handleObservableError = function (error) {
        return __WEBPACK_IMPORTED_MODULE_1_rxjs__["Observable"].throw(extractError(error));
    };
    BaseService.prototype.mapListKeys = function (list) {
        return list
            .snapshotChanges()
            .map(function (actions) { return actions.map(function (action) { return (__assign({ $key: action }, action.payload.doc.data())); }); });
    };
    return BaseService;
}());

//# sourceMappingURL=base.js.map

/***/ }),

/***/ 637:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__filter_filter__ = __webpack_require__(638);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__filter_by_filter_by__ = __webpack_require__(216);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__filter_filter__["a" /* FilterPipe */],
                __WEBPACK_IMPORTED_MODULE_2__filter_by_filter_by__["a" /* FilterByPipe */]
            ],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__filter_filter__["a" /* FilterPipe */],
                __WEBPACK_IMPORTED_MODULE_2__filter_by_filter_by__["a" /* FilterByPipe */]]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),

/***/ 638:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FilterPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FilterPipe = /** @class */ (function () {
    function FilterPipe() {
    }
    FilterPipe.prototype.transform = function (items, filter, isAnd) {
        if (filter && Array.isArray(items)) {
            var filterKeys_1 = Object.keys(filter);
            if (isAnd) {
                return items.filter(function (item) {
                    return filterKeys_1.reduce(function (memo, keyName) {
                        return (memo && new RegExp(filter[keyName], 'gi').test(item[keyName])) || filter[keyName] === "";
                    }, true);
                });
            }
            else {
                return items.filter(function (item) {
                    return filterKeys_1.some(function (keyName) {
                        return new RegExp(filter[keyName], 'gi').test(item[keyName]) || filter[keyName] === "";
                    });
                });
            }
        }
        else {
            return items;
        }
    };
    FilterPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'filter'
        })
    ], FilterPipe);
    return FilterPipe;
}());

//# sourceMappingURL=filter.js.map

/***/ }),

/***/ 639:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ComponentsModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__radio_radio__ = __webpack_require__(640);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__message_box_message_box__ = __webpack_require__(641);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


//import { AutoCompleteModule } from 'ionic2-auto-complete';


var ComponentsModule = /** @class */ (function () {
    function ComponentsModule() {
    }
    ComponentsModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__radio_radio__["b" /* RadioComponent */],
                __WEBPACK_IMPORTED_MODULE_2__radio_radio__["a" /* MdRadioGroup */],
                __WEBPACK_IMPORTED_MODULE_3__message_box_message_box__["a" /* MessageBoxComponent */]
            ],
            /*imports: [IonicModule, AutoCompleteModule],*/
            imports: [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* IonicModule */]],
            entryComponents: [],
            exports: [
                __WEBPACK_IMPORTED_MODULE_2__radio_radio__["b" /* RadioComponent */],
                __WEBPACK_IMPORTED_MODULE_2__radio_radio__["a" /* MdRadioGroup */],
                __WEBPACK_IMPORTED_MODULE_3__message_box_message_box__["a" /* MessageBoxComponent */]
            ]
        })
    ], ComponentsModule);
    return ComponentsModule;
}());

//# sourceMappingURL=components.module.js.map

/***/ }),

/***/ 640:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export MD_RADIO_GROUP_CONTROL_VALUE_ACCESSOR */
/* unused harmony export MdRadioChange */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MdRadioGroup; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return RadioComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(36);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};

//import {CommonModule} from '@angular/common';

var MD_RADIO_GROUP_CONTROL_VALUE_ACCESSOR = {
    provide: __WEBPACK_IMPORTED_MODULE_1__angular_forms__["b" /* NG_VALUE_ACCESSOR */],
    useExisting: Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* forwardRef */])(function () { return MdRadioGroup; }),
    multi: true
};
// TODO(mtlin):
// Ink ripple is currently placeholder.
// Determine motion spec for button transitions.
// Design review.
// RTL
// Support forms API.
// Use ChangeDetectionStrategy.OnPush
var _uniqueIdCounter = 0;
/** A simple change event emitted by either RadioComponent or MdRadioGroup. */
var MdRadioChange = /** @class */ (function () {
    function MdRadioChange() {
    }
    return MdRadioChange;
}());

var MdRadioGroup = /** @class */ (function () {
    function MdRadioGroup() {
        /**
         * Selected value for group. Should equal the value of the selected radio button if there *is*
         * a corresponding radio button with a matching value. If there is *not* such a corresponding
         * radio button, this value persists to be applied in case a new radio button is added with a
         * matching value.
         */
        this._value = null;
        /** The HTML name attribute applied to radio buttons in this group. */
        this._name = "md-radio-group-" + _uniqueIdCounter++;
        /** Disables all individual radio buttons assigned to this group. */
        this._disabled = false;
        /** The currently selected radio button. Should match value. */
        this._selected = null;
        /** Whether the `value` has been set to its initial value. */
        this._isInitialized = false;
        /** The method to be called in order to update ngModel */
        this._controlValueAccessorChangeFn = function (value) { };
        /** onTouch function registered via registerOnTouch (ControlValueAccessor). */
        this.onTouched = function () { };
        /** Event emitted when the group value changes. */
        this.change = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        /** Child radio buttons. */
        this._radios = null;
    }
    Object.defineProperty(MdRadioGroup.prototype, "name", {
        get: function () {
            return this._name;
        },
        set: function (value) {
            this._name = value;
            this._updateRadioButtonNames();
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MdRadioGroup.prototype, "disabled", {
        get: function () {
            return this._disabled;
        },
        set: function (value) {
            // The presence of *any* disabled value makes the component disabled, *except* for false.
            this._disabled = (value != null && value !== false) ? true : null;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MdRadioGroup.prototype, "value", {
        get: function () {
            return this._value;
        },
        set: function (newValue) {
            if (this._value != newValue) {
                // Set this before proceeding to ensure no circular loop occurs with selection.
                this._value = newValue;
                this._updateSelectedRadioFromValue();
                // Only fire a change event if this isn't the first time the value is ever set.
                if (this._isInitialized) {
                    this._emitChangeEvent();
                }
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(MdRadioGroup.prototype, "selected", {
        get: function () {
            return this._selected;
        },
        set: function (selected) {
            this._selected = selected;
            this.value = selected ? selected.value : null;
            if (selected && !selected.checked) {
                selected.checked = true;
            }
        },
        enumerable: true,
        configurable: true
    });
    /**
     * Initialize properties once content children are available.
     * This allows us to propagate relevant attributes to associated buttons.
     * TODO: internal
     */
    MdRadioGroup.prototype.ngAfterContentInit = function () {
        // Mark this component as initialized in AfterContentInit because the initial value can
        // possibly be set by NgModel on MdRadioGroup, and it is possible that the OnInit of the
        // NgModel occurs *after* the OnInit of the MdRadioGroup.
        this._isInitialized = true;
    };
    /**
     * Mark this group as being "touched" (for ngModel). Meant to be called by the contained
     * radio buttons upon their blur.
     */
    MdRadioGroup.prototype._touch = function () {
        if (this.onTouched) {
            this.onTouched();
        }
    };
    MdRadioGroup.prototype._updateRadioButtonNames = function () {
        var _this = this;
        if (this._radios) {
            this._radios.forEach(function (radio) {
                radio.name = _this.name;
            });
        }
    };
    /** Updates the `selected` radio button from the internal _value state. */
    MdRadioGroup.prototype._updateSelectedRadioFromValue = function () {
        var _this = this;
        // If the value already matches the selected radio, do nothing.
        var isAlreadySelected = this._selected != null && this._selected.value == this._value;
        if (this._radios != null && !isAlreadySelected) {
            var matchingRadio = this._radios.filter(function (radio) { return radio.value == _this._value; })[0];
            if (matchingRadio) {
                this.selected = matchingRadio;
            }
            else if (this.value == null) {
                this.selected = null;
                this._radios.forEach(function (radio) { radio.checked = false; });
            }
        }
    };
    /** Dispatch change event with current selection and group value. */
    MdRadioGroup.prototype._emitChangeEvent = function () {
        var event = new MdRadioChange();
        event.source = this._selected;
        event.value = this._value;
        this.change.emit(event);
    };
    /**
      * Implemented as part of ControlValueAccessor.
      * TODO: internal
      */
    MdRadioGroup.prototype.writeValue = function (value) {
        this.value = value;
    };
    /**
     * Implemented as part of ControlValueAccessor.
     * TODO: internal
     */
    MdRadioGroup.prototype.registerOnChange = function (fn) {
        this._controlValueAccessorChangeFn = fn;
    };
    /**
     * Implemented as part of ControlValueAccessor.
     * TODO: internal
     */
    MdRadioGroup.prototype.registerOnTouched = function (fn) {
        this.onTouched = fn;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], MdRadioGroup.prototype, "change", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["r" /* ContentChildren */])(Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_14" /* forwardRef */])(function () { return RadioComponent; })),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["T" /* QueryList */])
    ], MdRadioGroup.prototype, "_radios", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], MdRadioGroup.prototype, "name", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], MdRadioGroup.prototype, "align", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Object])
    ], MdRadioGroup.prototype, "disabled", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], MdRadioGroup.prototype, "value", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [RadioComponent])
    ], MdRadioGroup.prototype, "selected", null);
    MdRadioGroup = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["s" /* Directive */])({
            selector: 'md-radio-group',
            providers: [MD_RADIO_GROUP_CONTROL_VALUE_ACCESSOR],
            host: {
                'role': 'radiogroup',
            },
        })
    ], MdRadioGroup);
    return MdRadioGroup;
}());

var RadioComponent = /** @class */ (function () {
    function RadioComponent(radioGroup, _elementRef) {
        // Assertions. Ideally these should be stripped out by the compiler.
        // TODO(jelbourn): Assert that there's no name binding AND a parent radio group.
        this._elementRef = _elementRef;
        /** Whether this radio is checked. */
        this._checked = false;
        /** The unique ID for the radio button. */
        this.id = "md-radio-" + _uniqueIdCounter++;
        /** Value assigned to this radio.*/
        this._value = null;
        // set disableRipple(value) { this._disableRipple = coerceBooleanProperty(value); }
        /** Event emitted when the group value changes. */
        this.change = new __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */]();
        this.radioGroup = radioGroup;
        // radioDispatcher.listen((id: string, name: string) => {
        //   if (id != this.id && name == this.name) {
        //     this.checked = false;
        //   }
        // });
    }
    Object.defineProperty(RadioComponent.prototype, "disableRipple", {
        get: function () { return this._disableRipple; },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RadioComponent.prototype, "inputId", {
        get: function () {
            return this.id + "-input";
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RadioComponent.prototype, "checked", {
        get: function () {
            return this._checked;
        },
        set: function (newCheckedState) {
            this._checked = newCheckedState;
            if (newCheckedState && this.radioGroup && this.radioGroup.value != this.value) {
                this.radioGroup.selected = this;
            }
            else if (!newCheckedState && this.radioGroup && this.radioGroup.value == this.value) {
                // When unchecking the selected radio button, update the selected radio
                // property on the group.
                this.radioGroup.selected = null;
            }
            if (newCheckedState) {
                // Notify all radio buttons with the same name to un-check.
                // this.radioDispatcher.notify(this.id, this.name);
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RadioComponent.prototype, "value", {
        /** MdRadioGroup reads this to assign its own value. */
        get: function () {
            return this._value;
        },
        set: function (value) {
            if (this._value != value) {
                if (this.radioGroup != null && this.checked) {
                    this.radioGroup.value = value;
                }
                this._value = value;
            }
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RadioComponent.prototype, "align", {
        get: function () {
            return this._align || (this.radioGroup != null && this.radioGroup.align) || 'start';
        },
        set: function (value) {
            this._align = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(RadioComponent.prototype, "disabled", {
        get: function () {
            return this._disabled || (this.radioGroup != null && this.radioGroup.disabled);
        },
        set: function (value) {
            // The presence of *any* disabled value makes the component disabled, *except* for false.
            this._disabled = (value != null && value !== false) ? true : null;
        },
        enumerable: true,
        configurable: true
    });
    /** TODO: internal */
    RadioComponent.prototype.ngOnInit = function () {
        if (this.radioGroup) {
            // If the radio is inside a radio group, determine if it should be checked
            this.checked = this.radioGroup.value === this._value;
            // Copy name from parent radio group
            this.name = this.radioGroup.name;
        }
    };
    /** Dispatch change event with current value. */
    RadioComponent.prototype._emitChangeEvent = function () {
        var event = new MdRadioChange();
        event.source = this;
        event.value = this._value;
        this.change.emit(event);
    };
    /**
     * We use a hidden native input field to handle changes to focus state via keyboard navigation,
     * with visual rendering done separately. The native element is kept in sync with the overall
     * state of the component.
     */
    RadioComponent.prototype._onInputFocus = function () {
        this._isFocused = true;
    };
    /** TODO: internal */
    RadioComponent.prototype._onInputBlur = function () {
        this._isFocused = false;
        if (this.radioGroup) {
            this.radioGroup._touch();
        }
    };
    /** TODO: internal */
    RadioComponent.prototype._onInputClick = function (event) {
        // We have to stop propagation for click events on the visual hidden input element.
        // By default, when a user clicks on a label element, a generated click event will be
        // dispatched on the associated input element. Since we are using a label element as our
        // root container, the click event on the `radio-button` will be executed twice.
        // The real click event will bubble up, and the generated click event also tries to bubble up.
        // This will lead to multiple click events.
        // Preventing bubbling for the second event will solve that issue.
        event.stopPropagation();
    };
    /**
     * Triggered when the radio button received a click or the input recognized any change.
     * Clicking on a label element, will trigger a change event on the associated input.
     * TODO: internal
     */
    RadioComponent.prototype._onInputChange = function (event) {
        // We always have to stop propagation on the change event.
        // Otherwise the change event, from the input element, will bubble up and
        // emit its event object to the `change` output.
        event.stopPropagation();
        this.checked = true;
        this._emitChangeEvent();
        if (this.radioGroup) {
            this.radioGroup._controlValueAccessorChangeFn(this.value);
            this.radioGroup._touch();
        }
    };
    RadioComponent.prototype.getHostElement = function () {
        return this._elementRef.nativeElement;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* HostBinding */])('class.md-radio-focused'),
        __metadata("design:type", Boolean)
    ], RadioComponent.prototype, "_isFocused", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* HostBinding */])('id'),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], RadioComponent.prototype, "id", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String)
    ], RadioComponent.prototype, "name", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('aria-label'),
        __metadata("design:type", String)
    ], RadioComponent.prototype, "ariaLabel", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])('aria-labelledby'),
        __metadata("design:type", String)
    ], RadioComponent.prototype, "ariaLabelledby", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [])
    ], RadioComponent.prototype, "disableRipple", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["O" /* Output */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0__angular_core__["v" /* EventEmitter */])
    ], RadioComponent.prototype, "change", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* HostBinding */])('class.md-radio-checked'),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Boolean])
    ], RadioComponent.prototype, "checked", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], RadioComponent.prototype, "value", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], RadioComponent.prototype, "align", null);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["x" /* HostBinding */])('class.md-radio-disabled'),
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean),
        __metadata("design:paramtypes", [Boolean])
    ], RadioComponent.prototype, "disabled", null);
    RadioComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            // moduleId: module.id,
            selector: 'radio',template:/*ion-inline-start:"C:\Users\Kings\Desktop\project08092019\src\components\radio\radio.html"*/'\n<label [attr.for]="inputId" class="md-radio-label">\n  <!-- <div class="md-radio-container">\n    <div class="md-radio-outer-circle"></div>\n    <div class="md-radio-inner-circle"></div>\n    <div md-ripple *ngIf="!disableRipple" class="md-radio-ripple"\n         md-ripple-background-color="rgba(0, 0, 0, 0)"></div>\n  </div> -->\n\n  <input #input class="md-radio-input md-visually-hidden" type="radio"\n          [id]="inputId"\n          [checked]="checked"\n          [disabled]="disabled"\n          [name]="name"\n          [attr.aria-label]="ariaLabel"\n          [attr.aria-labelledby]="ariaLabelledby"\n          (change)="_onInputChange($event)"\n          (focus)="_onInputFocus()"\n          (blur)="_onInputBlur()"\n          (click)="_onInputClick($event)">\n\n  <!-- The label content for radio control. -->\n  <div class="md-radio-label-content" [class.md-radio-align-end]="align == \'end\'">\n    <ng-content></ng-content>\n  </div>\n</label>\n'/*ion-inline-end:"C:\Users\Kings\Desktop\project08092019\src\components\radio\radio.html"*/
        }),
        __param(0, Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["N" /* Optional */])()),
        __metadata("design:paramtypes", [MdRadioGroup,
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["t" /* ElementRef */]])
    ], RadioComponent);
    return RadioComponent;
}());

//# sourceMappingURL=radio.js.map

/***/ }),

/***/ 641:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MessageBoxComponent; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_messmodel__ = __webpack_require__(642);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MessageBoxComponent = /** @class */ (function () {
    function MessageBoxComponent() {
        this.time = false;
    }
    MessageBoxComponent.prototype.showTime = function () {
        //   if (this.time == true)
        //     this.time = false;
        //   else
        //     this.time = true
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1__models_messmodel__["a" /* Message */])
    ], MessageBoxComponent.prototype, "message", void 0);
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["D" /* Input */])(),
        __metadata("design:type", Boolean)
    ], MessageBoxComponent.prototype, "isFromSender", void 0);
    MessageBoxComponent = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'message-box',template:/*ion-inline-start:"C:\Users\Kings\Desktop\project08092019\src\components\message-box\message-box.html"*/'<p *ngIf="isFromSender && message.timestamp" >{{ message.timestamp.toDate() | date:\'hh:mm aaa\'}}</p>\n<div [ngClass]="{\'my_message\': isFromSender, \'other_message\': !isFromSender}">\n  <p>{{ message.text }}</p>\n</div>\n<p *ngIf="!isFromSender && message.timestamp">{{ message.timestamp.toDate() | date:\'hh:mm aaa\' }}</p>'/*ion-inline-end:"C:\Users\Kings\Desktop\project08092019\src\components\message-box\message-box.html"*/,
            host: {
                '[style.justify-content]': '((!isFromSender) ? "flex-start" : "flex-end")',
                '[style.text-align]': '((!isFromSender) ? "left" : "right")'
            }
        }),
        __metadata("design:paramtypes", [])
    ], MessageBoxComponent);
    return MessageBoxComponent;
}());

//# sourceMappingURL=message-box.js.map

/***/ }),

/***/ 642:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Message; });
var Message = /** @class */ (function () {
    function Message(base64, text, timestamp) {
        this.base64 = base64;
        this.text = text;
        this.timestamp = timestamp;
    }
    return Message;
}());

//# sourceMappingURL=messmodel.js.map

/***/ }),

/***/ 93:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_util_util__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase_app__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_firebase_app__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__angular_fire_auth__ = __webpack_require__(355);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__tabs_tabs__ = __webpack_require__(61);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_fire_firestore__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__global_global_variable__ = __webpack_require__(47);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__providers_app_app__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_select_country_select_country__ = __webpack_require__(357);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__ionic_storage__ = __webpack_require__(56);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};











var LoginPage = /** @class */ (function () {
    function LoginPage(storage, navCtrl, navParams, util, db, zone, appProvider, af_auth) {
        this.storage = storage;
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.util = util;
        this.db = db;
        this.zone = zone;
        this.appProvider = appProvider;
        this.af_auth = af_auth;
        this.countries = [];
        this.country = null;
    }
    LoginPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.itemsCollection = this.db.collection('users');
        this.util.presentLoading();
        this.getCountryList().then(function () {
            _this.util.stopLoading();
        });
    };
    LoginPage.prototype.getCountryList = function () {
        var _this = this;
        return new Promise(function (resolve) {
            _this.appProvider.getContryList().subscribe(function (data) {
                if (data) {
                    _this.countries = data;
                    __WEBPACK_IMPORTED_MODULE_7__global_global_variable__["a" /* GlobalVariables */].countries = data;
                }
                resolve();
            }, function (err) { return resolve(); });
        });
    };
    LoginPage.prototype.facebookLogin = function () {
        var _this = this;
        var TimeCreate = Date.now();
        facebookConnectPlugin.login(["public_profile", "email"], function (obj) {
            _this.util.presentLoading();
            var facebookCredential = __WEBPACK_IMPORTED_MODULE_3_firebase_app___default.a.auth.FacebookAuthProvider
                .credential(obj.authResponse.accessToken);
            _this.af_auth.auth.signInWithCredential(facebookCredential).then(function (res) {
                var data = res.toJSON();
                console.log(res.toJSON());
                var user = {};
                user['email'] = data['email'];
                user['name'] = data['displayName'];
                user['picture'] = data['photoURL'];
                user['base64'] = btoa(data['email']); //data['uid'];
                user['fb_id'] = data['providerData'][0]['uid'];
                user['online'] = true;
                user['typeMember'] = 'false';
                user['guide_fee'] = 'Free';
                // user['playerID'] = null;
                user['TimeCreate'] = TimeCreate;
                __WEBPACK_IMPORTED_MODULE_7__global_global_variable__["a" /* GlobalVariables */].user = user;
                var docRef = _this.itemsCollection.doc(user['base64']);
                docRef.get().subscribe(function (doc) {
                    _this.storage.set('checkUser', true);
                    if (!doc.exists) {
                        _this.itemsCollection.doc(user['base64']).set(user).then(function () {
                            _this.util.setLocal('user', user);
                            _this.storage.set('checkUser', true);
                        });
                    }
                    else {
                        __WEBPACK_IMPORTED_MODULE_7__global_global_variable__["a" /* GlobalVariables */].user = doc.data();
                        _this.util.setLocal('user', doc.data());
                        _this.storage.set('checkUser', true);
                    }
                    _this.util.stopLoading();
                    if (__WEBPACK_IMPORTED_MODULE_7__global_global_variable__["a" /* GlobalVariables */].user['country'] && __WEBPACK_IMPORTED_MODULE_7__global_global_variable__["a" /* GlobalVariables */].user['country'] != '') {
                        _this.zone.run(function () {
                            _this.storage.set('checkUser', true);
                            _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__tabs_tabs__["a" /* TabsPage */]);
                        });
                    }
                    else {
                        _this.util.showModal(__WEBPACK_IMPORTED_MODULE_9__pages_select_country_select_country__["a" /* SelectCountryPage */]).then(function (data) {
                            _this.storage.set('checkUser', true);
                        });
                    }
                });
            }, function (err) {
            });
        }, function (fail) {
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-login',template:/*ion-inline-start:"C:\Users\Kings\Desktop\project08092019\src\pages\login\login.html"*/'<ion-content>\n  <div class="login-wrap">\n    <h1>FACE TRIP</h1>\n    <button round full color="facebook" ion-button icon-left (tap)="facebookLogin()">\n      <ion-icon name="logo-facebook"></ion-icon>\n      Facebook\n    </button>\n    \n    <p text-center>Login or Register using your Facebook account</p>\n\n  </div>\n</ion-content>\n'/*ion-inline-end:"C:\Users\Kings\Desktop\project08092019\src\pages\login\login.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_10__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__providers_util_util__["a" /* UtilProvider */],
            __WEBPACK_IMPORTED_MODULE_6__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_0__angular_core__["M" /* NgZone */],
            __WEBPACK_IMPORTED_MODULE_8__providers_app_app__["a" /* AppProvider */],
            __WEBPACK_IMPORTED_MODULE_4__angular_fire_auth__["a" /* AngularFireAuth */]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 95:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(92);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_fire_firestore__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base__ = __webpack_require__(634);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__util_util__ = __webpack_require__(33);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var ChatProvider = /** @class */ (function (_super) {
    __extends(ChatProvider, _super);
    function ChatProvider(http, db, util) {
        var _this = _super.call(this) || this;
        _this.http = http;
        _this.db = db;
        _this.util = util;
        _this.chatCollections = _this.db.collection('Chats');
        _this.UserchatCollections = _this.db.collection('users');
        return _this;
    }
    ChatProvider.prototype.createRoomChat = function (userId, partner, chat) {
        this.chatCollections.doc('Room').collection(userId['base64']).doc(partner['base64']).set(chat);
    };
    ChatProvider.prototype.delectnull = function (userId, partner) {
        return (this.chatCollections.doc('Room').collection(userId).doc(partner).delete(),
            this.chatCollections.doc('Room').collection(partner).doc(userId).delete());
    };
    ChatProvider.prototype.getUserChat = function (userId) {
        return this.UserchatCollections.doc(userId);
    };
    ChatProvider.prototype.getDeepChat = function (userId1, userId2) {
        return this.chatCollections.doc('Room').collection(userId1['base64']).doc(userId2['base64']);
    };
    ChatProvider.prototype.getChatList = function (userID) {
        return this.chatCollections.doc('Room').collection(userID, function (res) { return res.orderBy('timestamp', "desc"); }).snapshotChanges();
    };
    ChatProvider.prototype.createMess = function (message, listMessages) {
        return Promise.resolve(listMessages.add(message));
    };
    ChatProvider.prototype.getMessages = function (userId, partner) {
        return this.chatCollections.doc('Messages').collection(userId + '-' + partner);
    };
    ChatProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */],
            __WEBPACK_IMPORTED_MODULE_2__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_4__util_util__["a" /* UtilProvider */]])
    ], ChatProvider);
    return ChatProvider;
}(__WEBPACK_IMPORTED_MODULE_3__base__["a" /* BaseService */]));

//# sourceMappingURL=chatprovider.js.map

/***/ }),

/***/ 96:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ChatsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(94);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__global_checkopenchat__ = __webpack_require__(360);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_app_chatprovider__ = __webpack_require__(95);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__ = __webpack_require__(27);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__ = __webpack_require__(217);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_rxjs_add_operator_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_onesignal__ = __webpack_require__(132);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_firebase_app__ = __webpack_require__(62);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_firebase_app___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_firebase_app__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var ChatsPage = /** @class */ (function () {
    function ChatsPage(navCtrl, navParams, db, https, app, oneSignal, chatProvider) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.db = db;
        this.https = https;
        this.app = app;
        this.oneSignal = oneSignal;
        this.chatProvider = chatProvider;
        this.listeners = [];
        this.viewMessages = [];
        this.receiver = {};
        this.user = {};
        this.receiver = Object.assign({}, this.navParams.get('receiver'));
        this.user = Object.assign({}, this.navParams.get('user'));
        this.chatCollections = this.db.collection('Chats');
    }
    ChatsPage.prototype.ionViewDidLeave = function () {
        var _this = this;
        __WEBPACK_IMPORTED_MODULE_3__global_checkopenchat__["a" /* CheckChatOpen */].checkOpen = false;
        __WEBPACK_IMPORTED_MODULE_3__global_checkopenchat__["a" /* CheckChatOpen */].checkNoti = '';
        //-------------------------Start Check Room Null-----------------------------------
        this.chatProvider.getDeepChat(this.user, this.receiver).get().subscribe(function (data) {
            var checkData = data.data();
            if (checkData['lastMessage'] === "") {
                _this.chatProvider.delectnull(_this.user['base64'], _this.receiver['base64']);
            }
            else {
                _this.seen();
            }
        });
        //-------------------------End Check Room Null-----------------------------------
    };
    ChatsPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        //-------------------------Start Chat-----------------------------------
        this.oneSignal.clearOneSignalNotifications();
        __WEBPACK_IMPORTED_MODULE_3__global_checkopenchat__["a" /* CheckChatOpen */].checkNoti = this.receiver['name'];
        __WEBPACK_IMPORTED_MODULE_3__global_checkopenchat__["a" /* CheckChatOpen */].checkOpen = true;
        this.chat1 = this.chatProvider.getDeepChat(this.user, this.receiver);
        this.chat2 = this.chatProvider.getDeepChat(this.receiver, this.user);
        this.messages = this.chatProvider.getMessages(this.receiver['base64'], this.user['base64']);
        this.messages.get().subscribe(function (data) {
            if (data.size === 0) {
                _this.checkUseSend = true;
                _this.messages = _this.chatProvider.getMessages(_this.user['base64'], _this.receiver['base64']);
                _this.getMessages(_this.user['base64'], _this.receiver['base64']);
                doSubscription();
            }
            else {
                _this.checkUseSend = false;
                _this.getMessages(_this.receiver['base64'], _this.user['base64']);
                doSubscription();
            }
        });
        var doSubscription = function () {
            _this.newMess = _this.chatProvider.mapListKeys(_this.messages);
            _this.newMess.subscribe(function (messages) {
                _this.scrollToBottom();
            });
        };
        this.chatProvider.getUserChat(this.receiver['base64']).get().subscribe(function (data) {
            _this.dataReceiver = data.data().playerID;
        });
        this.seen();
    };
    ChatsPage.prototype.submitChat = function (message) {
        var _this = this;
        var timestamp = __WEBPACK_IMPORTED_MODULE_8_firebase_app__["firestore"].FieldValue.serverTimestamp();
        if (message != '') {
            var messData = {
                'base64': this.user['base64'],
                'text': message,
                'timestamp': timestamp
            };
            this.chatProvider.createMess(messData, this.messages).then(function () {
                _this.chat1
                    .update({
                    lastMessage: message,
                    timestamp: timestamp
                });
                _this.chat2
                    .update({
                    lastMessage: message,
                    timestamp: timestamp,
                    seen: false
                });
                if (_this.dataReceiver) {
                    _this.sendNoti(message, _this.user, _this.dataReceiver);
                }
            }).then(function () {
                _this.message = null;
            });
        }
    };
    ChatsPage.prototype.doRefresh = function (refresher) {
        var _this = this;
        if (this.start == 0) {
            refresher.complete();
        }
        else {
            setTimeout(function () {
                if (_this.checkUseSend === true) {
                    _this.getMoreMessages(_this.user['base64'], _this.receiver['base64']);
                }
                else {
                    _this.getMoreMessages(_this.receiver['base64'], _this.user['base64']);
                }
                refresher.complete();
            }, 2000);
        }
    };
    ChatsPage.prototype.getMessages = function (userId1, userId2) {
        var _this = this;
        var getdata = this.chatCollections.doc('Messages').collection(userId1 + '-' + userId2);
        getdata.ref.orderBy('timestamp', 'desc').limit(15)
            .get().then(function (snapshots) {
            if (snapshots.docs.length > 0) {
                _this.start = snapshots.docs[snapshots.docs.length - 1];
            }
            else {
                _this.start = 0;
            }
            getdata.ref.orderBy('timestamp').startAt(_this.start)
                .onSnapshot(function (messages) {
                _this.viewMessages = [];
                messages.forEach(function (message) {
                    _this.viewMessages.push(message.data());
                });
            });
        });
    };
    ChatsPage.prototype.getMoreMessages = function (userId1, userId2) {
        var _this = this;
        var getdata = this.chatCollections.doc('Messages').collection(userId1 + '-' + userId2);
        getdata.ref.orderBy('timestamp', 'desc').startAt(this.start).limit(11)
            .get().then(function (snapshots) {
            _this.end = _this.start;
            _this.start = snapshots.docs[snapshots.docs.length - 1];
            getdata.ref.orderBy('timestamp').startAt(_this.start) //.endBefore(this.end)
                .onSnapshot(function (messages) {
                _this.viewMessages = [];
                messages.forEach(function (message) {
                    _this.viewMessages.push(message.data());
                });
            });
            _this.scrollToTop();
        });
    };
    ChatsPage.prototype.seen = function () {
        this.chat1
            .update({
            seen: true
        });
    };
    ChatsPage.prototype.sendNoti = function (message, idSend, receive) {
        var _this = this;
        var headers = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Headers */]({
            'Content-Type': 'application/json'
        });
        var options = new __WEBPACK_IMPORTED_MODULE_2__angular_http__["d" /* RequestOptions */]({ headers: headers });
        var data = JSON.stringify({
            app_id: "54940e0d-ca0d-4087-8e3c-57a70c54a20e",
            include_player_ids: [receive],
            headings: { en: "You have a message from " + idSend['name'] },
            contents: { en: message },
            data: { dataReceiver: this.user },
        });
        return new Promise(function (resolve, reject) {
            _this.https.post('https://onesignal.com/api/v1/notifications', data, options)
                .toPromise()
                .then(function (response) {
                resolve(response.json());
            })
                .catch(function (error) {
                console.error('API Error : ', error.status);
                console.error('API Error : ', JSON.stringify(error));
                reject(error.json());
            });
        });
    };
    ChatsPage.prototype.scrollToBottom = function (duration) {
        var _this = this;
        setTimeout(function () {
            if (_this.content._scroll) {
                _this.content.scrollToBottom(duration || 300);
            }
        }, 50);
    };
    ChatsPage.prototype.scrollToTop = function (duration) {
        var _this = this;
        setTimeout(function () {
            if (_this.content._scroll) {
                _this.content.scrollToTop(duration || 300);
            }
        }, 50);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])(__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["c" /* Content */])
    ], ChatsPage.prototype, "content", void 0);
    ChatsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-chats',template:/*ion-inline-start:"C:\Users\Kings\Desktop\project08092019\src\pages\chats\chats.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{receiver[\'name\']}}</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-refresher (ionRefresh)="doRefresh($event)">\n    <ion-refresher-content></ion-refresher-content>\n  </ion-refresher>\n\n  <message-box *ngFor="let msg of viewMessages" [message]="msg" [isFromSender]="(msg.base64 === user.base64)"></message-box>\n</ion-content>\n\n<ion-footer>\n  <ion-toolbar>\n    <ion-input type="text" (keyup.enter)="submitChat(message)" placeholder="Message..."\n      [(ngModel)]="message"></ion-input>\n    <ion-buttons end>\n      <button ion-button icon-only clear color="dark" (click)="submitChat(message); message=\'\'"\n        [disabled]="message == null">\n        <ion-icon name="md-send"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-toolbar>\n</ion-footer>'/*ion-inline-end:"C:\Users\Kings\Desktop\project08092019\src\pages\chats\chats.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_5__angular_fire_firestore__["a" /* AngularFirestore */],
            __WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_onesignal__["a" /* OneSignal */],
            __WEBPACK_IMPORTED_MODULE_4__providers_app_chatprovider__["a" /* ChatProvider */]])
    ], ChatsPage);
    return ChatsPage;
}());

//# sourceMappingURL=chats.js.map

/***/ })

},[365]);
//# sourceMappingURL=main.js.map