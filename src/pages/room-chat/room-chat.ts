import { Component } from '@angular/core';
import { NavController, AlertController, App } from 'ionic-angular';
import { AppProvider } from '../../providers/app/app';
import { ChatProvider } from '../../providers/app/chatprovider';
import { GlobalVariables } from '../../global/global_variable';
import { ChatsPage } from '../chats/chats';
import { NavParams } from 'ionic-angular/navigation/nav-params';
import { UtilProvider } from '../../providers/util/util';
import { FilterByPipe } from '../../pipes/filter-by/filter-by';
import { LoginPage } from '../login/login';
import { TabsPage } from '../tabs/tabs';

import { Storage } from '@ionic/storage';
import * as firebase from 'firebase/app';

@Component({
  selector: 'page-room-chat',
  templateUrl: 'room-chat.html',
})
export class RoomChatPage {
  timeNow;
  country: string = '';
  language: string = '';
  chatHistory: Array<any> = [];
  onlineUsers: Array<any> = [];
  countryCount: number = 0;
  chatHistorySearch: Array<any> = [];
  public checkUser: boolean;
  constructor(
    public app: App,
    public alertCtrl: AlertController,
    private storage: Storage,
    public navCtrl: NavController,
    public navParams: NavParams,
    private util: UtilProvider,
    private appProvider: AppProvider,
    private chatProvider: ChatProvider) {
  }
  ionViewWillEnter() {
    let timestamp: Object = firebase.firestore.FieldValue.serverTimestamp();
    this.storage.get('checkUser').then((val) => {
      if (val == true) {
        this.checkUser = true;
        this.util.presentLoading();
        this.appProvider.getOnlineUsers().then((data: any) => {
          this.onlineUsers = data;
          this.chatProvider.getChatList(GlobalVariables.user['base64']).subscribe(x => {
            this.chatHistory = [];
            x.forEach(element => {
              this.chatHistory.push(element.payload.doc.data());
            });
          })
          this.util.stopLoading();
        });
      } else {
        this.checkUser = false;
        this.showConfirm();
      }
    });
  }

  goToChat(user) {
    this.app.getRootNav().push(ChatsPage, { receiver: user, user: GlobalVariables.user });
  }

  onSearchClearCountry() {
    this.country = '';
    this.searchChatHistory();
  }
  onSearchClearLanguage() {
    this.language = '';
    this.searchChatHistory();
  }

  searchChatHistory() {
    var filterBy = new FilterByPipe();

    var filter = filterBy.transform(this.chatHistory, {
      country: this.country,
      languages: this.language
    });

    this.chatHistorySearch = filter;

  }
  showConfirm() {
    const confirm = this.alertCtrl.create({
      title: 'Login',
      message: 'Please login to continue !!!',
      buttons: [
        {
          text: 'Disagree',
          handler: () => {
            // this.navCtrl.push(HomePage);
            this.app.getRootNav().push(TabsPage);
          }
        },
        {
          text: 'Agree',
          handler: () => {
            this.app.getRootNav().push(LoginPage);
          }
        }
      ]
    });
    confirm.present();
  }
}