import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, ViewController, Content, ToastController } from 'ionic-angular';
import { AppProvider } from '../../providers/app/app';
import { ChatProvider } from '../../providers/app/chatprovider';
//import { UtilProvider } from '../../providers/util/util';
import { Http, Headers, RequestOptions } from '@angular/http';
import { ChatModel } from '../../models/chatmodel';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';

import * as firebase from 'firebase/app';

@Component({
  selector: 'page-popup',
  templateUrl: 'popup.html',
})
export class PopupPage {

  @ViewChild(Content) content: Content;
  itemsCollection: AngularFirestoreCollection<any>;
  message: string = '';
  messages;
  messData;
  receiver: any = {};
  myUser: any = {};
  msgList: Array<any> = [];
  listUser: any;
  chat1;
  chat2;
  chatCollections: AngularFirestoreCollection<any>;

  constructor(public navCtrl: NavController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public db: AngularFirestore,
    public https: Http,
    // private util: UtilProvider,
    private appProvider: AppProvider,
    public toastCtrl: ToastController,
    public chatProvider: ChatProvider) {
    this.chatCollections = this.db.collection<any>('Chats');
  }

  ionViewWillEnter() {
    let timestamp: Object = firebase.firestore.FieldValue.serverTimestamp();
    this.listUser = this.navParams.get('device');
    this.myUser = this.navParams.get('user');
    this.messData = {
      'base64': this.myUser['base64'],
      'text': this.message,
      'timestamp': timestamp
    }
  }

  clickSend() {
    if (this.message != '') {
      var i = 0;
      while (i < this.listUser.length) {
        this.createRoom(this.myUser, this.listUser[i], this.message);
        i += 1;
      }
      this.message = '';
      this.presentToast('successfully');
      this.viewCtrl.dismiss();

    }
  }

  createRoom(myUser, listUser, Mess) {
    let dataReceiver;

    this.chatProvider.getUserChat(listUser['base64']).get().subscribe(data => {
      dataReceiver = data.data().playerID;
    })

    this.chatProvider.getDeepChat(myUser, listUser).get().subscribe(x => {
      let checkRoom: Partial<ChatModel> = x.data()
      let timestamp: Object = firebase.firestore.FieldValue.serverTimestamp();

      let dataMess;
      if (!checkRoom) {
        let chat1 = { 'lastMessage': Mess, 'seen': false, 'timestamp': timestamp, 'name': listUser.name, 'picture': listUser.picture, 'base64': listUser.base64 };
        this.chatProvider.createRoomChat(myUser, listUser, chat1)
        let chat2 = { 'lastMessage': Mess, 'seen': false, 'timestamp': timestamp, 'name': myUser.name, 'picture': myUser.picture, 'base64': myUser.base64 };
        this.chatProvider.createRoomChat(listUser, myUser, chat2)
      } else {
        this.chat1 = this.chatProvider.getDeepChat(myUser, listUser);
        this.chat2 = this.chatProvider.getDeepChat(listUser, myUser);

        this.chat1
          .update({
            lastMessage: Mess,
            timestamp: timestamp
          });

        this.chat2
          .update({
            lastMessage: Mess,
            timestamp: timestamp,
            seen: false
          });
      }
      dataMess = this.chatProvider.getMessages(listUser['base64'], myUser['base64'])
      dataMess.get().subscribe(data => {
        if (data.size === 0) {
          dataMess = this.chatProvider.getMessages(myUser['base64'], listUser['base64'])
          this.sendMess(dataMess, Mess, myUser, dataReceiver);
        } else {
          this.sendMess(dataMess, Mess, myUser, dataReceiver);
        }
      })
    })
  }

  sendMess(dataMess, message, myUser, dataReceiver) {
    let timestamp: Object = firebase.firestore.FieldValue.serverTimestamp();
    let messData = {
      'base64': this.myUser['base64'],
      'text': message,
      'timestamp': timestamp
    }
    this.chatProvider.createMess(messData, dataMess).then(() => {

      if (dataReceiver) {
        this.sendNoti(message, myUser, dataReceiver);
      }
    })
  }

  sendNoti(message, idSend, receive) {
    let headers = new Headers(
      {
        'Content-Type': 'application/json'
      });
    let options = new RequestOptions({ headers: headers });

    let data = JSON.stringify({
      app_id: "54940e0d-ca0d-4087-8e3c-57a70c54a20e",
      include_player_ids: [receive],
      headings: { en: "You have a message from " + idSend['name'] },
      contents: { en: message },
      data: { dataReceiver: this.myUser },
    });
    return new Promise((resolve, reject) => {
      this.https.post('https://onesignal.com/api/v1/notifications', data, options)
        .toPromise()
        .then((response) => {
          resolve(response.json());
        })
        .catch((error) => {
          console.error('API Error : ', error.status);
          console.error('API Error : ', JSON.stringify(error));
          reject(error.json());
        });
    });
  }

  clickClose() {
    this.viewCtrl.dismiss();
  }
  presentToast(data) {
    const toast = this.toastCtrl.create({
      message: data,
      duration: 3000,
      position: 'middle'
    });
    toast.present();
  }

}
