import { Component, ViewChild } from '@angular/core';

import { MePage } from '../me/me';
import { RoomChatPage } from '../room-chat/room-chat';
import { HomePage } from '../home/home';
import { Tabs, Events } from 'ionic-angular';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {
  @ViewChild (Tabs) tabs:Tabs;
  
  tab1Root = HomePage;
  tab2Root = MePage;
  tab3Root = RoomChatPage;

  constructor(public events: Events) { }


}
