import { Component, ViewChild } from '@angular/core';
import { NavController, NavParams, Content, App, DateTime } from 'ionic-angular';
import { Http, Headers, RequestOptions } from '@angular/http';
import { CheckChatOpen } from '../../global/checkopenchat';
import { ChatProvider } from '../../providers/app/chatprovider';
import { AngularFirestoreDocument, AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import 'rxjs/add/operator/map'
import { Message } from '../../models/messmodel';

import { OneSignal } from '@ionic-native/onesignal';

import * as firebase from 'firebase/app';

@Component({
  selector: 'page-chats',
  templateUrl: 'chats.html',
})
export class ChatsPage {
  @ViewChild(Content) content: Content;
  newMess;
  listeners = [];
  start;
  end;
  dataReceiver: string;
  checkUseSend: boolean;
  message;
  messages;
  viewMessages = [];
  receiver: any = {};
  user: any = {};
  private chat1: AngularFirestoreDocument;
  private chat2: AngularFirestoreDocument;
  chatCollections: AngularFirestoreCollection<any>;


  constructor(public navCtrl: NavController, public navParams: NavParams,
    public db: AngularFirestore,
    public https: Http,
    public app: App,
    public oneSignal: OneSignal,
    private chatProvider: ChatProvider) {

    this.receiver = Object.assign({}, this.navParams.get('receiver'));
    this.user = Object.assign({}, this.navParams.get('user'));
    this.chatCollections = this.db.collection<any>('Chats');
  }

  ionViewDidLeave() {
    CheckChatOpen.checkOpen = false;
    CheckChatOpen.checkNoti = '';
    //-------------------------Start Check Room Null-----------------------------------
    this.chatProvider.getDeepChat(this.user, this.receiver).get().subscribe(data => {
      let checkData = data.data();
      if (checkData['lastMessage'] === "") {
        this.chatProvider.delectnull(this.user['base64'], this.receiver['base64']);
      }
      else {
        this.seen();
      }
    })
    //-------------------------End Check Room Null-----------------------------------

  }
  ionViewDidLoad() {
    //-------------------------Start Chat-----------------------------------
    this.oneSignal.clearOneSignalNotifications();
    CheckChatOpen.checkNoti = this.receiver['name'];
    CheckChatOpen.checkOpen = true;

    this.chat1 = this.chatProvider.getDeepChat(this.user, this.receiver);
    this.chat2 = this.chatProvider.getDeepChat(this.receiver, this.user);

    this.messages = this.chatProvider.getMessages(this.receiver['base64'], this.user['base64'])
    this.messages.get().subscribe(data => {
      if (data.size === 0) {
        this.checkUseSend = true;
        this.messages = this.chatProvider.getMessages(this.user['base64'], this.receiver['base64'])
        this.getMessages(this.user['base64'], this.receiver['base64'])
        doSubscription();
      } else {
        this.checkUseSend = false;
        this.getMessages(this.receiver['base64'], this.user['base64'])
        doSubscription();
      }
    })
    let doSubscription = () => {
      this.newMess = this.chatProvider.mapListKeys(this.messages);
      this.newMess.subscribe((messages: Message[]) => {
        this.scrollToBottom();
      });
    };

    this.chatProvider.getUserChat(this.receiver['base64']).get().subscribe(data => {
      this.dataReceiver = data.data().playerID;
    })
    this.seen();
  }

  submitChat(message) {
    let timestamp: Object = firebase.firestore.FieldValue.serverTimestamp();
    if (message != '') {
      let messData = {
        'base64': this.user['base64'],
        'text': message,
        'timestamp': timestamp
      }
      this.chatProvider.createMess(messData, this.messages).then(() => {
        this.chat1
          .update({
            lastMessage: message,
            timestamp: timestamp
          });

        this.chat2
          .update({
            lastMessage: message,
            timestamp: timestamp,
            seen: false
          });
        if (this.dataReceiver) {
          this.sendNoti(message, this.user, this.dataReceiver);
        }
      }).then(() => {
        this.message = null;
      })
    }
  }

  doRefresh(refresher) {
    if (this.start == 0) {
      refresher.complete();
    } else {
      setTimeout(() => {
        if (this.checkUseSend === true) {
          this.getMoreMessages(this.user['base64'], this.receiver['base64'])
        } else {
          this.getMoreMessages(this.receiver['base64'], this.user['base64'])
        }
        refresher.complete();
      }, 2000);
    }
  }

  getMessages(userId1, userId2) {
    let getdata = this.chatCollections.doc('Messages').collection(userId1 + '-' + userId2)
    getdata.ref.orderBy('timestamp', 'desc').limit(15)
      .get().then((snapshots) => {
        if (snapshots.docs.length > 0) {
          this.start = snapshots.docs[snapshots.docs.length - 1]
        }
        else {
          this.start = 0
        }
        getdata.ref.orderBy('timestamp').startAt(this.start)
          .onSnapshot((messages) => {
            this.viewMessages = [];
            messages.forEach((message) => {
              this.viewMessages.push(message.data())
            })
          })
      })
  }
  getMoreMessages(userId1, userId2) {
    let getdata = this.chatCollections.doc('Messages').collection(userId1 + '-' + userId2)
    getdata.ref.orderBy('timestamp', 'desc').startAt(this.start).limit(11)
      .get().then((snapshots) => {
        this.end = this.start
        this.start = snapshots.docs[snapshots.docs.length - 1]
        getdata.ref.orderBy('timestamp').startAt(this.start)//.endBefore(this.end)
          .onSnapshot((messages) => {
            this.viewMessages = [];
            messages.forEach((message) => {
              this.viewMessages.push(message.data())
            })
          })
        this.scrollToTop();
      })
  }
  seen() {
    this.chat1
      .update({
        seen: true
      });
  }

  sendNoti(message, idSend, receive) {
    let headers = new Headers(
      {
        'Content-Type': 'application/json'
      });
    let options = new RequestOptions({ headers: headers });

    let data = JSON.stringify({
      app_id: "54940e0d-ca0d-4087-8e3c-57a70c54a20e",
      include_player_ids: [receive],
      headings: { en: "You have a message from " + idSend['name'] },
      contents: { en: message },
      data: { dataReceiver: this.user },
    });
    return new Promise((resolve, reject) => {
      this.https.post('https://onesignal.com/api/v1/notifications', data, options)
        .toPromise()
        .then((response) => {
          resolve(response.json());
        })
        .catch((error) => {
          console.error('API Error : ', error.status);
          console.error('API Error : ', JSON.stringify(error));
          reject(error.json());
        });
    });
  }

  private scrollToBottom(duration?: number): void {
    setTimeout(() => {
      if (this.content._scroll) {
        this.content.scrollToBottom(duration || 300);
      }
    }, 50);
  }
  private scrollToTop(duration?: number): void {
    setTimeout(() => {
      if (this.content._scroll) {
        this.content.scrollToTop(duration || 300);
      }
    }, 50);
  }

  //-------------------------End Chat-----------------------------------

}
