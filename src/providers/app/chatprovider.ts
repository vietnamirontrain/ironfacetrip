import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { BaseService } from "./base";

import { UtilProvider } from '../util/util';
import { ChatModel } from '../../models/chatmodel';
import { Message } from '../../models/messmodel';

@Injectable()
export class ChatProvider extends BaseService {

    chatCollections: AngularFirestoreCollection<any>;
    UserchatCollections: AngularFirestoreCollection<any>;
    constructor(
        public http: HttpClient,
        public db: AngularFirestore,
        public util: UtilProvider
    ) {
        super();
        this.chatCollections = this.db.collection<any>('Chats');
        this.UserchatCollections = this.db.collection<any>('users');
    }

    createRoomChat(userId, partner, chat: ChatModel) {
        this.chatCollections.doc('Room').collection(userId['base64']).doc(partner['base64']).set(chat)
    }

    delectnull(userId: string, partner: string): Promise<void> {
        return (
            this.chatCollections.doc('Room').collection(userId).doc(partner).delete(),
            this.chatCollections.doc('Room').collection(partner).doc(userId).delete()
        );
    }

    getUserChat(userId: string):AngularFirestoreDocument {
        return this.UserchatCollections.doc(userId)
    }

    getDeepChat(userId1: string, userId2: string):AngularFirestoreDocument {
        return this.chatCollections.doc('Room').collection(userId1['base64']).doc(userId2['base64'])
    }

    getChatList(userID) {
        return this.chatCollections.doc('Room').collection(userID, res => res.orderBy('timestamp',"desc")).snapshotChanges();
    }

    createMess(message: Message, listMessages: AngularFirestoreCollection<Message>): Promise<any> {
        return Promise.resolve(listMessages.add(message));
    }
    getMessages(userId: string, partner: string) {
        return this.chatCollections.doc('Messages').collection(userId+ '-' + partner)
    }
}
