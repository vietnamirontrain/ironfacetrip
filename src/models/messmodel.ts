export class Message {

    constructor(
        public base64: string,
        public text: string,
        public timestamp: any
    ) {}

}