export class ChatModel {
    constructor(
        public lastMessage: string,
        public seen: boolean,
        public timestamp: any,
        public name: string,
        public picture: string,
        public base64: string
    ) { }

}