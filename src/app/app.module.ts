import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpModule } from '@angular/http';

import { OneSignal } from '@ionic-native/onesignal';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { UtilProvider } from '../providers/util/util';
import { IonicStorageModule } from '@ionic/storage';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AppProvider } from '../providers/app/app';
import { ChatProvider } from '../providers/app/chatprovider';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { PipesModule } from '../pipes/pipes.module';
import { ComponentsModule } from '../components/components.module';
import { Facebook } from '@ionic-native/facebook';
import { Camera } from '@ionic-native/camera';
import { WheelSelector } from '@ionic-native/wheel-selector';

import { MePage } from '../pages/me/me';
import { RoomChatPage } from '../pages/room-chat/room-chat';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { EditProfilePage } from '../pages/edit-profile/edit-profile';
import { HomestayDetailPage } from '../pages/homestay-detail/homestay-detail';
import { LocalDetailPage } from '../pages/local-detail/local-detail';
import { HomestayViewPage } from '../pages/homestay-view/homestay-view';
import { ChatsPage } from '../pages/chats/chats';
import { PopupPage } from '../pages/popup/popup';
import { SelectCountryPage } from '../pages/select-country/select-country';


export const firebaseConfig = {
  apiKey: "AIzaSyBvEJmRrEhzkoirQaTTJEYLnVLVZZ6urHM",
  authDomain: "facetriper.firebaseio.com",
  databaseURL: "https://facetriper.firebaseio.com",
  projectId: "facetriper",
  storageBucket: "facetriper.appspot.com",
  messagingSenderId: "443594649686"
};

@NgModule({
  declarations: [
    MyApp,
    MePage,
    RoomChatPage,
    HomePage,
    TabsPage,
    LoginPage,
    EditProfilePage,
    HomestayDetailPage,
    LocalDetailPage,
    HomestayViewPage,
    ChatsPage,
    PopupPage,
    SelectCountryPage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      tabsHideOnSubPages: false,
    }),
    IonicStorageModule.forRoot({
      name: '_facetrip',
      driverOrder: ['indexeddb', 'sqlite', 'websql'],
    }),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    PipesModule,
    ComponentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    MePage,
    RoomChatPage,
    HomePage,
    TabsPage,
    LoginPage,
    EditProfilePage,
    HomestayDetailPage,
    LocalDetailPage,
    HomestayViewPage,
    ChatsPage,
    PopupPage,
    SelectCountryPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    UtilProvider,
    AppProvider,
    ChatProvider,
    Facebook,
    Camera,
    WheelSelector,
    OneSignal
  ]
})
export class AppModule { }
