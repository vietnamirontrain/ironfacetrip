import { Component } from '@angular/core';
import { App, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { TabsPage } from '../pages/tabs/tabs';
import { AppProvider } from '../providers/app/app';
import { GlobalVariables } from '../global/global_variable';
import { CheckChatOpen } from '../global/checkopenchat';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { UtilProvider } from '../providers/util/util';

import { OneSignal } from '@ionic-native/onesignal';

import { Storage } from '@ionic/storage';

import { ChatsPage } from '../pages/chats/chats';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  countryList: Array<any> = [];
  itemsCollections: AngularFirestoreCollection<any>;
  signal_app_id: string = '54940e0d-ca0d-4087-8e3c-57a70c54a20e';
  firebase_id: string = '443594649686';
  checkNoti;

  constructor(private storage: Storage, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen,
    // private toastCtrl: ToastController,
    private app: App,
    oneSignal: OneSignal,
    private util: UtilProvider,
    private appProvider: AppProvider, private db: AngularFirestore) {

    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();

      this.getCountryList().then(() => { });
      this.itemsCollections = this.db.collection('users');
      this.util.getLocal('user').then(user => {
        if (user) {
          this.itemsCollections.doc(btoa(user['email'])).get().subscribe(snap => {
            if (snap.exists) {
              GlobalVariables.user = snap.data();
              var usr = snap.data();
              usr['online'] = true;
              this.itemsCollections.doc(GlobalVariables.user['base64']).update(usr).then(() => {
              });
              storage.set('checkUser', true);
              this.rootPage = TabsPage;
            }
            else {
              storage.set('checkUser', false);
              this.rootPage = TabsPage;
            }
          })
        } else {
          storage.set('checkUser', false);
          this.rootPage = TabsPage;
        }
      })


      /*++++++++++++++++++++++++++ Start Notification +++++++++++++++++++++++++++*/
      oneSignal.startInit(this.signal_app_id, this.firebase_id);
      oneSignal.inFocusDisplaying(oneSignal.OSInFocusDisplayOption.Notification);

      oneSignal.handleNotificationOpened().subscribe((jsonData) => {
        if (CheckChatOpen.checkOpen == true) {
          this.app.goBack()
        }
        setTimeout(() => {
          this.app.getRootNav().push(ChatsPage, {
            receiver: jsonData.notification.payload.additionalData.dataReceiver,
            user: GlobalVariables.user
          });
          oneSignal.clearOneSignalNotifications();
          this.checkNoti = jsonData.notification.payload.title
        }, 3000);
      });

      oneSignal.handleNotificationReceived().subscribe((res) => {
        let checkMe='You have a message from ' + CheckChatOpen.checkNoti;
        if (CheckChatOpen.checkOpen == true && res.payload.title === checkMe) {
          oneSignal.clearOneSignalNotifications();
        }
        // let toast = this.toastCtrl.create({
        //   message: res.payload.title,
        //   duration: 3000,
        //   position: 'top'
        // });
        // toast.present();
      });
      oneSignal.endInit();

      /*++++++++++++++++++++++++++ End Notification +++++++++++++++++++++++++++++*/
    });
  }

  getCountryList() {
    return new Promise(resolve => {
      this.appProvider.getContryList().subscribe((data: any) => {
        if (data) {
          this.countryList = data;
          GlobalVariables.countries = data;
        }
        resolve();
      }, err => resolve());
    })
  }
}
