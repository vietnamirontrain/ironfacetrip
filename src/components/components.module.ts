import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
//import { AutoCompleteModule } from 'ionic2-auto-complete';
import { RadioComponent, MdRadioGroup} from './radio/radio';
import { MessageBoxComponent } from './message-box/message-box';
@NgModule({
	declarations: [
	RadioComponent,
	MdRadioGroup,
    MessageBoxComponent],
	/*imports: [IonicModule, AutoCompleteModule],*/
	imports: [IonicModule],
	entryComponents: [],
	exports: [
	RadioComponent,
	MdRadioGroup,
    MessageBoxComponent]
})
export class ComponentsModule { }
