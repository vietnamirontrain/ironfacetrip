import { Component, Input } from '@angular/core';
import { Message } from '../../models/messmodel';

@Component({
  selector: 'message-box',
  templateUrl: 'message-box.html',
  host: {
    '[style.justify-content]': '((!isFromSender) ? "flex-start" : "flex-end")',
    '[style.text-align]': '((!isFromSender) ? "left" : "right")'
  }
})
export class MessageBoxComponent {
  time: boolean = false;
  @Input() message: Message;
  @Input() isFromSender: boolean;

  constructor() { }
  showTime() {
  //   if (this.time == true)
  //     this.time = false;
  //   else
  //     this.time = true
  }
}
